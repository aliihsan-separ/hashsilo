<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api', 'throttle:60,1')->group(function () {
    Route::prefix('v1')->group(function () {
        Route::get('/{ticker}/block/average', "RPCClientController@getBlockAverage");
        Route::get('/{ticker}/coin', "RPCClientController@getCoin");
        Route::get('/{ticker}/coin/history', "RPCClientController@getCoinHistory");
        Route::get('/{ticker}/masternode', "RPCClientController@getMasternodes");
        Route::get('/{ticker}/masternode/count', "RPCClientController@getMasternodeCount");
        Route::get('/{ticker}/masternode/average', "RPCClientController@getMasternodeAverage");
        Route::get('/{ticker}/peer', "RPCClientController@getPeer");
        Route::get('/{ticker}/supply', "RPCClientController@getSupply");
        Route::get('/{ticker}/top100', "RPCClientController@getTop100");
        Route::get('/{ticker}/difficulty', "RPCClientController@getDifficulty");
        Route::get('/{ticker}/block/count', "RPCClientController@getBlockCount");
    });
});
