<?php
//URL::forceScheme('https');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes();

Route::get('/coin/{coin_name}','CoinController@show')->name('coin.name');

Route::middleware(['auth'])->group(function () {

    Route::get('/admin/login', 'Auth\LoginController@login')->name('admin.login');

    Route::get('/home', 'CoinController@index')->name('home');

    Route::resource('coins','CoinController');

    Route::get('/coin/social/{id}','CoinController@social')->name('coin.social');
    Route::post('/coin/social/store','CoinController@social_store')->name('social.store');
    Route::post('/coin/social/update/{id}','CoinController@social_update')->name('social.update');

});
