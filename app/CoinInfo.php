<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinInfo extends Model
{
    protected $table = "coin_infos";

    protected $fillable = ["coin_id","symbol","name"];

    public $timestamps = false;
}
