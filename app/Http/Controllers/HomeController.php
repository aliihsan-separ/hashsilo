<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $coin_cache = \Cache::remember('coins', 10, function () {
            $coin = [];
            $daily_volume = 0;
            $getMarketSummaries = json_decode(file_get_contents('https://www.coinexchange.io/api/v1/getmarketsummaries'), true);

            foreach ($getMarketSummaries['result'] as $summary) {
                $coin[$summary['MarketAssetCode']] = $summary;
                $daily_volume += $summary['Volume'];
            }

            $data = [
                'coins' => $coin,
                'coin_count' => count($coin),
                'daily_volume' => $daily_volume,
            ];
            return $data;
        });

        return view('home')->with($coin_cache);
    }
}
