<?php

namespace App\Http\Controllers;

use App\Coin;

class WelcomeController extends Controller
{
    public function index()
    {
        $url = 'https://bitpay.com/api/rates';
        $json = json_decode(file_get_contents($url));
        $dollar = $btc = 0;

        foreach ($json as $obj) {
            if ($obj->code == 'USD') $btc = $obj->rate;
        }

        $coins = Coin::all();
        $data = [];
        $monitored_masternode = 0;
        $dominance = 0;
        $dominance_coin = '';
        $total_volume = 0;
        $dominance_percent = 0;
dd(1);
        if (count($coins)) {
            foreach ($coins as $coin) {
               /* $mn_count = json_decode(file_get_contents($coin->mn_count_api_url), true);
                $exchange_url = json_decode(file_get_contents($coin->exchange_api_url), true);
                $circulation = json_decode(file_get_contents($coin->circulation_supply_api_url), true);*/

                $monitored_masternode += $mn_count['result']['active'];
                $volume[$coin->name] = $exchange_url[strtolower($coin->name)]['usd_24h_vol'];
                $market_cap[$coin->name] = $exchange_url[strtolower($coin->name)]['usd'] * $circulation;
                $price[$coin->name] = $exchange_url[strtolower($coin->name)]['usd'];
                $change[$coin->name] = $exchange_url[strtolower($coin->name)]['usd_24h_change'];
                $daily_reward[$coin->name] = $coin->paid_rewards_mn / $mn_count['result']['total'];
                $roi_days[$coin->name] = ($coin->paid_rewards_mn / $daily_reward[$coin->name]);
                $roi_percent[$coin->name] = 365 * 100 / ($coin->paid_rewards_mn / $daily_reward[$coin->name]);
                $mn_worth[$coin->name] = $coin->mn_required_coins * $exchange_url[strtolower($coin->name)]['usd'];


                if ($dominance < $volume[$coin->name]) {
                    $dominance = $volume[$coin->name];
                    $dominance_coin = $coin->name;
                }
                $total_volume += $volume[$coin->name];
            }

            $dominance_percent =  $dominance*100 / $total_volume;

            $data = [
                'btc' => $btc,
                'monitored_masternode' => $monitored_masternode,
                'coins' => $coins,
                'usd_24h_vol' => $volume,
                'market_cap' => $market_cap,
                'usd' => $price,
                'usd_24h_change' => $change,
                'daily_reward' => $daily_reward,
                'roi_days' => $roi_days,
                'roi_percent' => $roi_percent,
                'mn_worth' => $mn_worth,
                'dominance_percent' => $dominance_percent,
                'dominance_coin' => $dominance_coin,
            ];
        } else {
            $data = [
                'btc' => $btc,
                'dominance_percent' => $dominance_percent,
                'dominance_coin' => $dominance_coin,
            ];
        }

        return view('welcome')->with($data);
    }
}
