<?php

namespace App\Http\Controllers;

use App\Coin;
use App\CoinSocial;
use App\CoinInfo;
use App\Foundation\RPCClient;
use Illuminate\Http\Request;

class CoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coins = Coin::all();
        $data = [];

        if (count($coins)) {
            $api = new RPCClientController();

            foreach ($coins as $coin) {
                $circulation = json_decode($api->getSupply($coin->coin_id)->getContent())->supply;
                $mn_count = $api->getMiddleware($coin->coin_id);
                dd($circulation);

                $curl_handle1=curl_init();
                curl_setopt($curl_handle1, CURLOPT_URL,$coin->exchange_api_url);
                curl_setopt($curl_handle1, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle1, CURLOPT_USERAGENT, 'HASH');
                $query_exchange = curl_exec($curl_handle1);
                curl_close($curl_handle1);

                $exchange = json_decode($query_exchange,true);


                $volume[$coin->name] = $exchange[$coin->id]['usd_24h_vol'];
                $market_cap[$coin->name] = $exchange[$coin->id]['usd'] * $circulation;
                $price[$coin->name] = $exchange[$coin->id]['usd'];
                $change[$coin->name] = $exchange[$coin->id]['usd_24h_change'];
                $daily_reward[$coin->name] = $coin->paid_rewards_mn / $api->getMasternodeCount($coin->ticker);
                $roi_days[$coin->name] = ($coin->paid_rewards_mn / $daily_reward[$coin->name]);
                $roi_percent[$coin->name] = 365 * 100 / ($coin->paid_rewards_mn / $daily_reward[$coin->name]);
                $mn_worth[$coin->name] = $coin->mn_required_coins * $exchange[strtolower($coin->name)]['usd'];
            }
            dd(1);

            $data = [
                'coins' => $coins,
                'usd_24h_vol' => $volume,
                'market_cap' => $market_cap,
                'usd' => $price,
                'usd_24h_change' => $change,
                'daily_reward' => $daily_reward,
                'roi_days' => $roi_days,
                'roi_percent' => $roi_percent,
                'mn_worth' => $mn_worth
            ];
        }
        return view('home')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_coin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'      =>  'required|min:4',
            'ticker'    =>  'required|min:2',
            'paid_rewards_mn'   =>  'required|min:6',
            'mn_required_coins' =>  'required|min:3',
            'icon_url' =>   'required|min:6',
            'masternode_reward_per_block'   =>  'required',
            'block_time'    =>  'required',
            'algorithm' =>  'required',
            'rpc_host'  =>  'required',
            'rpc_port'  =>  'required',
            'rpc_user'  =>  'required',
            'rpc_password'  =>  'required',
        ];

        $this->validate($request, $rules);

        $real_ticker = CoinInfo::where('symbol','like','%'.$request->ticker.'%')->first();

        if ($real_ticker){
            $exchange_url = "https://api.coingecko.com/api/v3/simple/price?ids={$real_ticker->coin_id}&vs_currencies=usd&include_24hr_vol=true&include_24hr_change=true&include_last_updated_at=true";

        }else {
            $real_ticker = CoinInfo::where('coin_id','like','%'.$request->ticker.'%')->first();
            $exchange_url = "https://api.coingecko.com/api/v3/simple/price?ids={$real_ticker->coin_id}&vs_currencies=usd&include_24hr_vol=true&include_24hr_change=true&include_last_updated_at=true";

        }

        $request->request->add(["exchange_api_url"=>$exchange_url, "coin_id" => $real_ticker->coin_id]);

        $data = request()->except("_token");

        $coin = Coin::firstOrCreate($data);

        if ($coin) {
            return redirect()->route('home')->with("message", $request->name . " Inserted Successfull");
        }

        return redirect()->back()->with("error", "Something went wrong");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $url = 'https://bitpay.com/api/rates';
        $json = json_decode(file_get_contents($url));
        $dollar = $btc = 0;

        foreach ($json as $obj) {
            if ($obj->code == 'USD') $btc = $obj->rate;
        }
        $coin = Coin::whereName($name)->first();
        return view('show_coin', compact('coin', 'btc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coin = Coin::find($id);
        return view('edit_coin', compact('coin'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coin = Coin::find($id);
        $coin->name = $request->name;
        $coin->ticker = $request->ticker;
        $coin->exchange_api_url = $request->exchange_api_url;
        $coin->paid_rewards_mn = $request->paid_rewards_mn;
        $coin->mn_required_coins = $request->mn_required_coins;
        $coin->circulation_supply_api_url = $request->circulation_supply_api_url;
        $coin->mn_count_api_url = $request->mn_count_api_url;
        $coin->icon_url = $request->icon_url;
        $coin->save();

        return redirect()->route('coins.index')->with("message", $coin->name . " Update Successfull");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coin = Coin::destroy($id);

        if ($coin) {
            return redirect()->back()->with('message', 'Coin Deleted Successful');
        }

        return redirect()->back()->with("error", "Something went wrong");
    }

    public function social($id)
    {
        $coin = Coin::find($id);
        return view('create_social', compact('coin'));
    }

    public function social_store(Request $request)
    {
        $data = request()->except("_token", "coin_name");

        $social = CoinSocial::firstOrCreate($data);

        if ($social) {
            return redirect()->route('home')->with("message", $request->coin_name . " Social Medias Of The Coin Successfully Inserted");
        }

        return redirect()->back()->with("error", "Something went wrong");
    }

    public function social_update(Request $request, $id)
    {
        $update = CoinSocial::updateOrCreate([
            'coin_id' => $request->coin_id,
        ], [
            'twitter' => $request->twitter,
            'btc_talk' => $request->btc_talk,
            'explorer' => $request->explorer,
            'github' => $request->github,
            'website' => $request->website,
            'exchange1' => $request->exchange1,
            'exchange_name1' => $request->exchange_name1,
            'exchange2' => $request->exchange2,
            'exchange_name2' => $request->exchange_name2,
            'exchange3' => $request->exchange3,
            'exchange_name3' => $request->exchange_name3,
        ]);
        if ($update) {
            return redirect()->route('home')->with("message", $request->coin_name . " Social Medias Of The Coin Successfully Updated");
        }

        return redirect()->back()->with("error", "Something went wrong");
    }
}
