<?php

namespace App\Http\Controllers;

use App\Foundation\RPCClient;
use App\Coin;

class RPCClientController extends Controller
{

    protected $client;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBlockAverage($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            $last_height = $this->client->call('getblockcount', []);
            $last_block = $this->client->call('getblock', [$this->client->call('getblockhash', [$last_height - 1])])["time"];
            $last2_block = $this->client->call('getblock', [$this->client->call('getblockhash', [$last_height - 2])])["time"];
            return response()->json($last_block - $last2_block);
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCoin($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            return response()->json(collect($coin)->only('name','ticker','paid_rewards_mn','mn_required_coins','masternode_reward_per_block','block_time','algorithm'));
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCoinHistory($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return response()->json(["status" => "ok"]);
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMasternodes($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return collect($this->client->call('masternode', ['list']))->sortByDesc('lastpaid')->sortBy('activetime')->values();
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMasternodeCount($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return $this->client->call('masternode', ['count']);
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMasternodeAverage($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return $this->client->call('masternode', ['count']);
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPeer($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return $this->client->call('getpeerinfo', []);
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSupply($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();

        if ($coin) {
            $data = json_decode(file_get_contents('https://api.coingecko.com/api/v3/simple/price?ids=' . $ticker . '&vs_currencies=usd&include_market_cap=true&include_24hr_vol=true&include_24hr_change=true&include_last_updated_at=true'),true);
            if (isset($data[$ticker]["usd_market_cap"])) {
                return response()->json(['supply' => $data[$ticker]["usd_market_cap"] / $data[$ticker]["usd"]]);
            }
        }
        return response()->json(["error" => true, "message" => "ticker name not found"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTop100($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return response()->json(["status" => "ok"]);
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDifficulty($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
        if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return response()->json($this->client->call('getdifficulty', []));
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBlockCount($ticker)
    {
        $coin = Coin::where("coin_id", $ticker)->first();
	if ($coin) {
            $this->client = new RPCClient($coin->rpc_host, $coin->rpc_port, $coin->rpc_user, $coin->rpc_password);
            return response()->json($this->client->call('getblockcount', []));
        } else {
            return response()->json(["error" => true, "message" => "ticker name not found"]);
        }
    }

}
