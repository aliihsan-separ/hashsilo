<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ticker');
            $table->string('coin_id');
            $table->string('exchange_api_url');
            $table->string('paid_rewards_mn');
            $table->string('mn_required_coins');
            $table->string('masternode_reward_per_block'); // elle
            $table->string('block_time'); // elle
            $table->string('algorithm'); // elle
            $table->string('icon_url');
            $table->string('rpc_host')->nullable(); //  elle
            $table->string('rpc_port')->nullable(); //  elle
            $table->string('rpc_user')->nullable(); //  elle
            $table->string('rpc_password')->nullable(); //  elle
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
