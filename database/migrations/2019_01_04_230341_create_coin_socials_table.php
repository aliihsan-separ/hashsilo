<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_socials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coin_id')->index()->unique();
            $table->string('website')->nullable();
            $table->string('btc_talk')->nullable();
            $table->string('github')->nullable();
            $table->string('explorer')->nullable();
            $table->string('twitter')->nullable();
            $table->string('exchange1')->nullable();
            $table->string('exchange_name1')->nullable();
            $table->string('exchange2')->nullable();
            $table->string('exchange_name2')->nullable();
            $table->string('exchange3')->nullable();
            $table->string('exchange_name3')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_socials');

    }
}
