<?php

use App\CoinInfo;
use Illuminate\Database\Seeder;

class CoinSymbolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //https://api.coingecko.com/api/v3/coins/list
        $curl_handle1=curl_init();
        curl_setopt($curl_handle1, CURLOPT_URL,"https://api.coingecko.com/api/v3/coins/list");
        curl_setopt($curl_handle1, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle1, CURLOPT_USERAGENT, 'MNBROWSING1');
        $query_exchange = curl_exec($curl_handle1);
        curl_close($curl_handle1);
        $coin_infos= json_decode($query_exchange,true);

        for ($i=0; $i<count($coin_infos);$i++) {
            $coin_info = new CoinInfo();
            $coin_info->coin_id = $coin_infos[$i]["id"];
            $coin_info->symbol = $coin_infos[$i]["symbol"];
            $coin_info->name = $coin_infos[$i]["name"];
            $coin_info->save();
        }

    }
}
