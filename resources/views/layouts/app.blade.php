<!doctype html>
<!--[if lte IE 9]>
<html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Hashsilo</title>

    <meta name="description" content="Hash">
    <meta name="author" content="Ryan Dev">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Hash">
    <meta property="og:site_name" content="Hash">
    <meta property="og:description" content="Hash">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{!! asset('images/icon.png') !!}">
    <link rel="icon" type="image/png" sizes="192x192" href="{!! asset('images/icon.png' ) !!}">
    <link rel="apple-touch-icon" sizes="180x180"  href="{!! asset('images/icon.png') !!}">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
    <!-- Fonts and Codebase framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{!! asset('assets/css/codebase.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.css') !!}">
</head>
<body>

<div id="page-container" class=" page-header-modern main-content-boxed">
    <header id="page-header" style="background-color: white !important;">
        <!-- Header Content -->
        <div class="content-header">
            <!-- Left Section -->
            <div class="content-header-section">
                <h2 class="mt-20">
                    <a href="{!! route('welcome') !!}"><img src="{!! asset('images/logo-i.png') !!}" alt="{!! config('app.name') !!}" width="150"></a>
                </h2>
                <div class="btn-group" role="group"> </div>
            </div>
            <!-- END Left Section -->
        </div>
        <!-- END Header Content -->
    </header>
    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="content">
            <div class="my-50 text-center">
                @yield('content')
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="opacity-0">
        <div class="content py-20 font-size-xs clearfix">
            <div class="float-right">
                Developed by <a class="font-w600" href="javascript:void(0)" target="_blank">GG Dev</a>
            </div>
            <div class="float-left">
                <a class="font-w600" href="https://goo.gl/po9Usv" target="_blank">Copyright</a> &copy; <span
                        class="js-year-copy">2019</span> {!! config('app.name') !!}
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
<script src="{!! asset('assets/js/core/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery-scrollLock.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.appear.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.countTo.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/js.cookie.min.js') !!}"></script>
<script src="{!! asset('assets/js/codebase.js') !!}"></script>
<!-- Page JS Plugins -->
<script src="{!! asset('assets/js/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') !!}"></script>
<!-- Page JS Code -->
<script src="{!! asset('assets/js/pages/be_tables_datatables.js') !!}"></script>

<script src="{!! asset('assets/js/plugins/chartjs/Chart.bundle.min.js') !!}"></script>

<!-- Page JS Code -->
{{--<script src="{!! asset('assets/js/pages/be_pages_crypto_dashboard.js') !!}"></script>--}}
</body>
</html>