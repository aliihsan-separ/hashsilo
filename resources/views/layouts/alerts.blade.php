@if($errors->hasBag())
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $key=>$error)
            <div>{!! $error !!}
                @if($key==0)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                @endif
            </div>
        @endforeach
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success alert-dismissible">
        <i class="fa fa-check"></i> {{ session()->get('message') }}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

@if(session()->has('warning'))
    <div class="alert alert-warning alert-dismissible">
        <i class="fa fa-exclamation-triangle"></i> {{ session()->get('warning') }}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger alert-dismissible">
        <i class="fa fa-remove"></i> {{ session()->get('error') }}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif