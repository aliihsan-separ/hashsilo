<!doctype html>
<!--[if lte IE 9]>
<html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Hashsilo</title>

    <meta name="description" content="Hash">
    <meta name="author" content="Ryan Dev">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Hash">
    <meta property="og:site_name" content="Hash">
    <meta property="og:description" content="Hash">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{!! asset('images/icon.png') !!}">
    <link rel="icon" type="image/png" sizes="192x192" href="{!! asset('images/icon.png' ) !!}">
    <link rel="apple-touch-icon" sizes="180x180"  href="{!! asset('images/icon.png') !!}">
    <!-- END Icons -->

    <!-- Stylesheets -->

    <!-- Fonts and Codebase framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{!! asset('assets/css/codebase.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.css') !!}">
</head>
<body>

<div id="page-container" class=" page-header-modern main-content-boxed">
    <header id="page-header" style="background-color: white !important;">
        <!-- Header Content -->
        <div class="content-header">
            <!-- Left Section -->
            <div class="content-header-section">
                <h2 class="mt-20">
                    <img src="{!! asset('images/logo-i.png') !!}" alt="{!! config('app.name') !!}" width="150">
                </h2>
                <div class="btn-group" role="group"> </div>
            </div>
            <!-- END Left Section -->
        </div>
        <!-- END Header Content -->
    </header>
    <!-- Main Container -->
    <main id="main-container">

        <!-- Page Content -->
        <div class="content">
            <div class="my-50 text-center">

                <div class="row js-appear-enabled animated fadeIn" data-toggle="appear">

                    <div class="col-6 col-xl-3">
                        <a class="block block-link-shadow text-left" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="font-size-h3 font-w600  js-count-to-enabled"><span data-toggle="countTo" data-speed="1000" data-to="{!! isset($coins) ? count($coins) : 0 !!}">{!! isset($coins) ? count($coins) : 0 !!}</span></div>
                                    </div>
                                    <div class="col-2">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAJzElEQVR4nO2dTYxb1RXHf+e+5yQEwoRqBEknJDRdlZBFZkXAsZ4tK4tC2cAAbdRNq7ZQFValIlFRvahKpHYFtNAvpKoqgQaQSqGLyJrxOBPCJomUILqoAk0mU5KS5mtIMoPte7p4Nngc2+NxbM97nveTRhrf9+Fz33nvvnv/555rYYmQTqc3qupXS6XSkIisB9YBQ8BtwI3AMuBmwAFWlw+7ABSBaeAz4DJwBpgCTqnqScdxpkTkeDab/bATdkonThI0PM+7A7jXGLPFWjssIlv44iJ3iwuqesQYc9haewQ4kMvl/r3Qk/SFQzKZjBkfH98iIt9Q1fuBYYJRtw+Bt1X179PT0+OHDh0qzHdAEIxum23btq11Xfdx4Pv4TU+QOQP8rlgsvrh///6PG+0USockEonNjuPsBB7Eb/tb4YKqHjPGnLDWnjLGTAEngDOqetFxnEKxWLxULBZLExMTFwDi8fhq13Ud13VvLpVKMREZUNU1IrLeWjtkjFlnrd0gIptpvUn8DHijVCo9m8/nj9VuDJVDtm7desOKFSt2Az8E3Ca7FlR1whjzrqoecRznSKdeuo1Ip9MbS6XSFmBYVbeKSByINTmkCPxmZmbm6YMHD16tFIbGIZ7n3WGMeUtVNzfY5Tzwloi8Y4zZl81mL/bSvlrS6fSAtXa7qt4HPADcUm8/ETlmrX2g0gEIhUM8z7tDRMaB9XU2fyAiz7uu++d9+/Zd7rVtrbB9+/Ybi8Xit1X1CeDOOrucFJHE6OjoicA7xPM8V0T2A3fXbPoYeGJsbOxNQHtvWVuI53kPisjzwJqabe+p6jazGFYtkB1c64zXCoXCprGxsTcIjzMANJfLvV4oFO4EXqvZdjewIwxPSF5EtlU+i8iro6Oj3yJcjqiHJJPJPcAjVWX5wD8hIrKppujHhN8ZAFoqlZ6qKbsr8A6hputorQ38U30d2DA4ZI7cYIz5JSHpHTYjk8kYx3F+VVM8E/iKJZPJes3Ta4VC4fGJiYnzPTeoA8Tj8VtisdiLzH1/ABCGJ6Qej8RisQ88z3uIcD0tkkqlRmKx2AfUcQaEoDINnpDPEZF/WWt/vWzZsj8EdWDoed4KEXkYeAq4q9m+oXdIFYGSTjzPWy0i24Gv00Q6qSV0DlHVn4rILmBlk8MKwAHgXVU94rru4Ww2+xHd6y5LKpXaaK3dIiLDwFYgTnMB9LKqPisiP59zoi4Z2DFqHTI2NiaJROJ2x3F2Aw/TvNLVXASO4UvuUyJySlVPAqeNMdMiMmOM+fTKlSuFdDp9ESCbzQ6sXLkyZq29SVVXWGtXAWtEZL2qVkLAG4DNwECLdhSAv5ZKpZ35fH6ytn6hdEjl/1QqNWStfUxEvkcIAlSq+ntjzEujo6NTlcLa+rV6dwWScsWeyWQyP4tCuD2i2RPSiHQ6vd5ae295gsMwsIUWX6rXwXngcGWigzHmQDabPTnfQX31hDSifCFOAnsqZel0emOxWNwIDInIBvz2fwj/nbBSVZcDq/CvyTXTgERkVlWvAKfxpwFNqeoJYMpaezyfz3/UCdv70iH1KIdwuxrG7QRhHan3LWFwyHT1B8/z1i2WIZ0mkUjcXlM0HQaHLCW1txD4ikVqbziI1N7FIlJ7A0ak9gaMSO0NGJHa2yZtZCgJ/kWywKdlQy8B/8FXRqeMMf9V1eeqv6ff1d62HLJIGUoVnlfVA8aYiUrFRkZGnLNnz96tqvcbY+5rMiG71xxV1X+IyNuDg4Pv7d27t1S7Q1sOGRkZcT755JOtxpj7y7O5m/YUeshx4C3gb4ODgxOVCqdSqQ3APVVq7zDdv2E6ovY2dUg/ZSgFVe1tySH9nKEUNJo6ZClkKAWNhg5ZKhlKQaLcfa9+z0wLLK0MpV7b1ohyKvce/LFUhXOy1DKUcrlccTEMq2Y+tXdJZSgtgk3VzKv2usB35xzRBxlK5TjJN5PJJFRVXES+A/yp1/ZUmlTgSVX9WrN93X7PUHIcp/pO7NmAtlrtLRQKLau9LkssQ6lL5+2Y2utSP2Yd6iYLPu/F1MasY8lk8mXglIh8rKpT+APX/7muax3HuXD58mWdmJg4n8lkzELUXlUdEGnpXq5Ve+fK70stZr2ItKT2Noqp923MusccVdXdQHxwcHAol8s9U+2MejRr49aIyN5UKvVP4LkwDAxpoRfTRdpSe2tp1GQ1+sLASCdtx6xFnrTW3ioiQ8CtIvJlVR3AvzlX4bcIq/Hfob1Xe5dazLqV2fO9ZF6H9HvMOugOqXux8/n8JLAjlUr9ZAEx6wH8uzYOoPrF91jrd/9LpRKxWIzx8XEAYrEYhcK1OSzVxy6Aur2YILPgub2jo6NTuVzumcHBwSEgrqq7RSRIQZ8F92KCQqO5vS01R+VY9YHy384wZSgFkXg8fsv4+PiLzJXeAb7UVsJOmDOUFhlJpVIPlac21YYHgA5mUIUlQ2kxqJ7bq6pNBc4lk9LWa9odJ7n4GUqrqk60LpfLneqKlT2mTi/mbJe+KlJ756OB2rvC87w3RWRSRCbx30+TIjLtOM707OzsVVWdWbt27aVNmzZppPZ2iEjtDQ6BU3uBZ4nU3kXhPHAIOKKqh621B8qKx4KI1N7Gaq/F1+cK+OkSV4GrInJaVau1uilVPd7uRLxI7Q24uBipvYtMpPYGiDrjpHMLUnvxewxHu2VgGyy4FxMUGqi977ej9u5Kp9Pri8XiPVXZScP0Ru297l5MEGii9v7xetXeVytliUTiK47jbFTVdeWkzyH8xM/bgBvKfzfhT8yrvA+63osJGA3VXlU9CLzSMXGxLI/3g0TecVoYJ51wXffRbDZbjNTeLtHq3N5KQlEl2BapvZ2hnXFSUVVfmJmZ2VWdcrfk1N5kMvm6iJyy1k7ij5cmgU97qPZ+BuxV1d25XO792o2R2ts7zgC/LRaLLzX9YckmWtZpVX0il8uFKYtq3ph1jzkKvAO802glh1rmFRdFJFJ7WyNSe/tB7b3GvkjtDRaR2hsw6jqk8n8/rkcVdIc0vfv78dcHgk5LzVEmk7H4PYhDQCZSe7tHpPYGjEjtDRhhXdm6bzHAueqCPv/1gXN1dwwQBpijOPb5rw9co64GDaOqL1cXqOqjyWRyTzwe73avqWuUY9avUCdmvRj2LAQD/KUcz62m7+b2qupBVX1lkexqGQF/nVtVHceXKebu0B9q7wnHcRJhyEkM7SKYraq9QV4Esx7XLBO7fPnyX4jIj5hnmViCr/YWVfWF2dnZXUFeJraWuu8Hz/PuEpGngRFaX0g5KGpv05h10GllqfHHgB8QArWXFmLWQaelHlR5Bkek9vaAti5qpPZ2j47d5ZHa2xn+D1pFKau64sAZAAAAAElFTkSuQmCC" alt="" width="25">
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Coins listed</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-6 col-xl-3">
                        <a class="block block-link-shadow text-left" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="font-size-h3 font-w600 js-count-to-enabled"><span data-toggle="countTo" data-speed="1000" data-to="{!! isset($monitored_masternode)? $monitored_masternode : 0 !!}">{!! isset($monitored_masternode) ? $monitored_masternode : 0 !!}</span></div>
                                    </div>
                                    <div class="col-2">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAEPklEQVR4nO3dv28bZRgH8O9jkzIkYfJEOkFVJAoSJMzJezdEihAiVPJQkbHqwtpQEIsnRJq/AKpuzVC5rVxRlgz2yUtbQfghqhapYuPH4iVJO9SR72GoI12PVHHvPTfP2d+PlOF9bT053zev3/uRuwOIiIiIKH9y1AvwPEEQ6DDrt1otk5+9dNQLQM9iIMYwEGNeOeoFGJTvd/6w56S8cIQYw0CMYSDG5DqHVKvVY51OZxnAMoBZAMcBTOb5O4blBeaYxwD+AvAzgEalUmnU6/VuXsuR2wgJguB0p9N5AOAqgDMA3kJBwnhBk3j62c4AuNrpdO6HYfhJXsW9A6nVaqUgCC4CuA7gDf9FKpw3VfVGEARrtVrNe316F4iiaA3Aqm+dEfB5u93+2reI1xwSBMFpAOdT3U8AfAdgY2Ji4t7m5ubjjLVf6n7DoPs5i4uLk3t7e++o6oqInANwbP81Vb3gnLsTRVEj63JkDqQ/ga+nuv8G8GGr1fota13r+n9gdwHcdc5dFpFbAGb2XxeR9bm5uR+2trb2stTP/JXV35pKzhlPMOJhpEVR9Guv1/sIQHIr68T09PTHWWv6zCHLqfa34xTGvna7/QuAS8k+EUmvm4H5zCFzqYXY8Kh1KMvHolT1ioh8luj6IGstnxHyerIRx/F9j1pFdy/VnjnwXQPwCWQq2Yii6JFHrUI74LNPHfjGAfBYljFmz4dYPec9bBwhxjAQYxiIMQzEGAZiDAMxhoEYw0CMYSDGMBBjGIgxDMQYBmLM/46ojuuVS8/zstcHR4gxDMQYBmLMoWcMx+XKpUENe31whBjDQIxhIMYwEGMYiDEMxBgGYgwDMYaBGMNAjGEgxhx6LGvUjkX5Gvb64AgxhoEYw0CIiIhGRObzw+nt8aL9v1Xe8lof3MoyhoEYw0CMYSDGMBBjGIgxDMQYBmIMAzHG7P2yxvVKLo4QY3wCeeY+g865zPcZLLqlpaXXUl07WWv5BPJPsiEipzxqFVq3201/9n+z1vKZQ34CcDLR/hRPb8E9FJav5IrjeCXZFpEfs9byGSE3U+1zzrn3POoVUhiGswDOJvviOE6vm4FlDqRSqTQA/JnoelVEbs3Pz7+ftWbRhGE4q6rfI/GEBBF5uLu7mzmQzF9Z9Xq9G4bhqqreSHTPlMvlO0EQXCqVShu9Xu/3UbvBsnNuqlwuvxvH8YqqnkUiDACqquezPhkByOFZuP2n6wz9gS55zyHpennMMar6TRRFX/rU8N4PWVhY+ALARd86Bacisuac+8q3kPeeeq1WiwFccM7dFpF1ACd8axaJiDxU1dVms3mz2Wx618ttTz2KosbOzs7bqloFsAHgD6R2HkfEIxF5ICJXVLW6vb19qtVqZZ7E03I9ltWfzK71f7xYfQbVsPFYljEMxBgGYozZ8yFp43IlF0eIMQzEGAZCRERERGPgPwQoO5FG1rJ1AAAAAElFTkSuQmCC" alt="" width="25">
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Monitored masternodes</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-6 col-xl-3">
                        <a class="block block-link-shadow text-left" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="font-size-h3 font-w600 js-count-to-enabled"><span data-toggle="countTo" data-speed="1000" data-to="{!! $btc !!}">{!! $btc !!}</span></div>
                                    </div>
                                    <div class="col-2">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAHyklEQVR4nO2dbYwVVxnHf88AldW2NNmFKKnKItoYWTWlSd3QyJm7RWOJ1hYwpDZt0yoQsaFW/dCkqWu0JmrT0sZqoFiJoTFxEYlQW4E7M1CRVElNsWpFEShqarm+1L4t+zKPH2aWXebenTv0zt3Z7j2/T3fPffY8f+aZOed/zpwsYLFYLBaLxWLJH5nohK7rXgVsBBCR1Z7nPTbRGtIoWp8zkcliNgIXAxer6sYC8tejUH1FFMSSwoQXRERWAyeBk6q6eqLz12Oy67NYpi7d3d1txpjpaTHWZSVohj5jzEIRuQ8wRNOE7zjO7eVy+XAy1rqsanLV57ruJSLyBHAlMJ3omveEYfiE67rvS8Zbl9V8vgJcVKP9QuDuZKN1WQny1GeMOR/4ZErIkqr8jSS0pGOMWSEifSkhFd/3Z49tsENWExGRa+qE/CrZkGrBmkGruKyVK1eeV6lUlqXFiMiOZJt1WdXkou/UqVMlYFZKyFAYhjuTjXbIahIZhqtfBkFQSTZal5UgD329vb0O8Ik6YT+tmf/1JHyj4rruzcB64F1ACJwAds6YMWPD7t27X8grjzHmingxOB4qIp2e551IftEqQ5YYY+4Dvg+8H3gLcAGwELhjcHDwd67r9uSWTOS6OiG/rVUMaAGXZYyZLiKbgRtTwuYAj7uuuw74G/AQENbSt2jRohmzZs1aq6o3AfOBGcCfROQH7e3t3wOmVSqVT6VpEpGawxUUUBBGXQyxi3lHE3OJiGwBPp0hdjqRtn5gJlTri4u7TVWT88OlqnpppVK5XkS2Au1picIw3JYmYspijPkW2YoxlpljPg+O/UJE7iV9sr5cVS+v0/+vgyB4drwvp6zLcl33CyLypUb6EJE7Rz4bY64Hbm1Ul6r+MDVnowkmI8aYVSLyCI3fcM+p6nIRGSTa5nhzg/0NOI4zt1wu/2u8gClXkFKpVFLVx4DzcuqyH6gQz3sNst33/eVpAVPKZRljFqrqT8hWjNeAtgxxM8mnGACpwxVMob2spUuXzhWRR6n9MijJXsdxOoG9eeXPwDFVfbReUFOeEGPMlSLSC3yQaFj8C7AD2A7omFCt/u1qXNddDtwBvJvorj4KbJo2bdrDe/fufXHx4sUXDA8P7yKbhX5qYGDg2gMHDrxkjPmYiNwPfC7rv60B7gmCYKheUO5zSKlUuk1V703pe5BoMXVSVW8IgiAYr6+enp7uMAy/DSweJ+Ql4EdEWyFZVtpHHcdZXC6X/zm20RizPra05zpivBpP+l8DLkuJ+2t/f//CgwcPvlavw2nnKCAVY8wtwHdJL/RIzvNFZE5nZyddXV1Hjxw5csbzG2MWzJ8/f6Oq3kP6Xf8mYBHRirkeL4iI63neyeQXx48ff3LevHmHRWRZ3GcmVPXmIAi2d3V1PTIwMLCAaCsmyZDjOCv379//5yx95vaEGGMuizfUZtYNruZlot3PbUR3+lryc0kArziO45bL5d+kBbmue0msodaFTfKA7/vrx/wspVJpnap+huiJHQIOi8hXPc/zsgpNK4iUSqU1qroGWEC0DfGcqnoi8rP29vagr69vAMAY0yEih4B3Zk08gQyp6jVBEOzKEuy67jJgJynXRkR2hGG4MsuccK7UTNrb2+vs27dvE3BLyu/+T0R2h2H4CxG5Ebgib3E5scb3/U1Zg13X/TswN/5xGPg90Q15GjiqqluAjc0oBtR2WRIEwYMiklYMgAtVdYWIrGiGsDwQkbs9z8tcjJhwzOd/+L7/gTw11aPKVRhjviEiaydSRDNQ1Q2e591ZP/Jsin6jedaQFZ8j+nGyvUH+DewBPk7je0E/B8oicpOqdo0XJCL3e553W4O5CuHMhe/p6XlPGIaHiN6k5cWQ4zhXlcvlPfEpvqvjt2kf5dws9zOq+uUgCB4faYjf8N1AtEZ5G9Ei85iqbg6C4AEyLjonG2cK4rruHqIDwbmhqp8PguDBsW3x5t8W4O2MLhLH4wRw15IlS7b29vaGKXG5UfS5sekQ2VayrXQzo6rfSRYjbt9CVAyA54mGsmuBq4m2RoZV9RkR2dzR0bG1r69vwPf9PKXVYyLfaFYx4rIuos68ISK7gCfDMPyIiHSTsg8mIts6OjrGG8PPyuP7/tPA00SnxFseB2D27NnHiCbfmojIH0+fPn2d53lfD4Lgw4ODg3NUdVX8vvpZ4BXgP8AhEbm1vb19VV9f3/A4fU35c1kN5R/5EG+wbagR86KqfijtPbAlP844nXiD7eV4OBrZYPuDiKyIhxXLBFA1b3R3d7e1tbW9NwzDV5vxVBTtYupRtL6qiTnes3+qiTkLdTEZKFRfqxwlfcMwZc9lvV4muz6LpbWxf8khQdH6psy5rByxfy/LMop1WQkmuz6LpbWxLitB0fqsy6rGuizLKNZlJZjs+iyW1sa6rARF67MuqxrrsiyjWJeVYLLrs1haG+uyEhStz7qsaqzLsoxiXVaCya7PYmltrMtKULQ+67KqsS7LMop1WQkmuz6LpbWxLitB0fqsy6rGuizLKNZlJZjs+iyW1sa6rARF67Muq5pC9VU9IcaYL4rIXUT/m7GleRwTkXXJJ/CsJ8QY81YR+Sa2GBNBp6o+lGysNWTZtUmBnHXxgyB4XkRuB/5bkJ5W4piIfLZoERaLxWKZOvwfATmS2nM5kQoAAAAASUVORK5CYII=" alt="" width="25">
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">BTC</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-6 col-xl-3">
                        <a class="block block-link-shadow text-left" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="font-size-h3 font-w600 js-count-to-enabled"><span data-toggle="countTo" data-speed="1000" data-to="{!! number_format((float)$dominance_percent, 2, '.', ',') !!}">{!! number_format((float)$dominance_percent, 2, '.', ',') !!} </span>%</div>
                                    </div>
                                    <div class="col-2">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGgAAABoCAYAAAAdHLWhAAAABmJLR0QA/wD/AP+gvaeTAAAIf0lEQVR4nO3da2wcVxUH8P+5M16vYzuxm+YhhaZOSADRKIDMQ46Jc2cMSVMRCggLVAkQoiGkRRRVESmPIlMpFZFQoaDSEoUvLUKV0g+oUKI02ZmxDZiHQhFFkdKUxAkRJY2LHT/X8cwcPuyss3Ecr2PP7Nr4/L54vXPn3rNzZnbn3r0zCwghhBBCCCHE4mKUO4CFoK2tzVi5cuWahoaGkZ6enrCUbatSNrYQWZb1nt7e3rNE9C8iOq+1/mwp26dSNrYQaa1fJqKPTnr6RdM09x4/fvzfSbcvR1ARRLRhiqc/7vv+Kdu2dyPhnVwSVNzNErCMmQ9prTtaW1vfkVTjkqA5IqKtYRj+TWu9v62tLfaTLklQPKqI6Pu9vb2/11pvirNiSVC8PkREr1iW9aTWuiaOCiVB8TMBfI2I/q61vnuulUmCkrOOiI7atv1rrfXbZluJJChhzPwxInpVa/1Qe3v7LW9vSVBp1BHRjzo6Orpu9SRCElRaW4jor5ZlPd7U1FQ1kxUW9VCP1tr0fb8WAEzTrDZNMxWGITFzHQAwcxURvQBgVQLN/5OIHnAc5+XpCpU0Qbt27VrS19dXSUTpysrKKt/3U0RUDcBk5lrDMCY2ThiGy4hIEVEqDMNqAFBK1QIwwzA0iGhpVG01gBQRTawLIA0gv4fWIfc6U1FZAKhF7mxrPvilUurhTCZzaaqF1yWoubm51jCM1aZprmDmOiKqDsNwKRFVMnMtgBoiqgSwDEAlgCVEVMHMNcxcuNHqo7/LkHsbnU8bZD7qI6L9juMcBsCFCwjIJSaVSj0P4J5yRCcm/E4ptSeTyZzKP2EAwIYNG54D8ImyhSXy1jLz7vXr16dXr17dffHiRZ+2bt26wjTNNyDfrs43rwdBcI8yTXMtJDnz0QbDML6tmHlRn2rPdyoIgvMASjoRQhRHRGeCIDigurq6LgP4TbkDEhPGADwWhuHmzs7OMyYA+L5/v2maRwE0lje2xY2ZXSLa67ru6fxzE58/O3furBwdHd0I4HYAtxPRbQW9+LRSqoqZlyDXQa0BUJHvpEZVVCHXgweu75jmO61m9Ly40ZtEtM9xnF9gqo5qqWmt077vVwGAUqomlUpVAEAQBBPJjEYuAABEVE1EKQAIw1AR0bL8MmYu3DEAoE4pRVHZSiJaUlDPUmY2ovVMIqotqGeiDSJSzJxvYy2SGwVhIvp5RUXF/mPHjv13qgJyBleEZVnnADQkUPVrRLTXcRxnukIyPlZ6I8z82ODg4BMnT54cL1ZYElRaLzHzVz3P65npCpKg0ngDwCOu6z57qyvKN6rJCgEcSqfT75pNcgA5ghJDRK8A+IrjOH+eSz1yBMVvmJkfWb58+QfmmhxAjqBYEdGvDMN4MM7LUiRB8bgE4BuO48zqc2Y6kqC5O8LMD3ie15tE5ZKg4m72Vcx5AHtc1z2WZONyklDcq5P+DwH8hJk3JZ0cQI6goojoIWZeAWALM/9FKfV1x3H+UO64hBBCCCGEEEIIIW5U0mlXtm1vYeYfIjeD9Y+GYdx34sSJC6WMYaEpyVic1rqGiB5n5gdxbYC2OQzDJwF8shQxLFSJJ0hrfTcRPQPgzsnLmHlz0u0vdIklSGtdR0QHAUx30zv5uqOIRBJk2/YuZn4awJok6l9MYk2QbdtrmPkpZr43znoXs1jeYtrb25VlWV9m5lMAJDkxmvMRpLV+b0dHx88AfDCGeMQks07Q9u3bq33ff5SZ90GuEk/MrBJk2/au8fHxpwDcEXM8YpJbSlB0EvBjZv5UUgGJ680oQY2NjRW1tbUPM/N3ASwpuoKITdEEWZbVDOAZALHebljMzE0HS3fs2HHb+Pj4QWb+0nTl5sgHcAEAiOgKM4cAwMxXiWg4X4iZB4nIj8oFzDxQsGxEKTUGAGEYMoD+gvqzRDRaUPaKUmqiDWaeaIOIBqN4YBhGXz6+bDY7CABBEGS7u7tHUWJTbXiyLOtzAH4AYEWJ41kohgFcjR5fQW62aRg9nryDjQAYK9ixxoloiJnHmHkk2oGyAPqZ+U0AvVVVVWeOHj06BkxKkGVZ72Tmp4nISvwliumc9H1/Z1dX12UTyN23gIi+CWB/dEdFUV6NpmkeBnAvtbS0bDRN8yVm3ljuqMR1QmZepQzD+I4kZ15SRHSnwqR7w4j5RQVBcADA6+UORNwgUEr1qM7OzjPZbHYzER3AtVNHUX4vZDKZtyafZt+F3KjBh8sTk4j8lpk/43ne0JQdVdu272fmg7h2rzdxo3EAQwACAAPIfZbnRzH6o/8HouVDyHVQ+/IdVKXUAHId1CEAQ0TUHwTBZQD/8TxvKN/ITYdwWltbV4Vh+ASA++J/bbPiAxiMHud78oUbJd8jBxH1MzNHZYYBgJkHlFIBAD8Mw0EAUEoNR0M+oVLqSn7dIAhYKTXAzAGAIcMwxn3fH/F9f6zUQz5Fx9hs297OzD8F8PYE2r/EzJ/Oj5cRUb9Sin3fv+r7/jAAmKY56Hmen0DbC8KMBkGbmpqq0un0owD2AaiIsf0e13XXxVjf/50ZTRrp7u4edV33W0qpTczsJh2UuOaWZvVkMpnXPM9rBfAFAG8lE5IoNJtpV+y67rNKqbsAPBd3QOJ6s54Xl8lkLrmu+3lmtgCcLrqCmJU5T1z0PM/LZrPvI6LvIXfXdBGjWGaWdnd3jzqO087M7wfwpzjqFDmxXl3ged4/tm3btgXAHuR6yGKOErvCrqWlZZ1hGIcAfGSaYtIPKiKx63M6OzvPua67nZl3I5pMMQX5LqqIpC+gYs/zDpum+W4AL06x/EzC7S94JZn0fvbs2cGenp7nGxoaThNRM3K/gnJBKfXFc+fOTfn7oSKn5D+u0djYWFFfX39HfX39+SNHjgSlbl8IIYQQQgghxPz2P5SOI5m/LycOAAAAAElFTkSuQmCC" alt="" width="25">
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Dominance Coin : {!! $dominance_coin !!}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer"
                                   id="DataTables_Table_1" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1"
                                        colspan="1" aria-label="Name: activate to sort column ascending">Name
                                    </th>
                                    <th class="d-none d-sm-table-cell sorting" tabindex="0"
                                        aria-controls="DataTables_Table_1" rowspan="1" colspan="1">
                                        VOLUME
                                    </th>
                                    <th class="d-none d-sm-table-cell sorting_desc" style="width: 15%;" tabindex="0"
                                        aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                        aria-label="Access: activate to sort column ascending" aria-sort="descending">
                                        MARKET CAP
                                    </th>
                                    <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                                        aria-label="Profile">PRICE
                                    </th>
                                    <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                                        aria-label="Profile">CHANGE
                                    </th>
                                    <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                                        aria-label="Profile">ROI
                                    </th>
                                    <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                                        aria-label="Profile">MN WORTH
                                    </th>
                                </tr>
                                </thead>
                                <tbody>


                                @if(isset($coins) && count($coins))
                                    @foreach($coins as $coin)
                                        <tr role="row" class="odd">
                                            <td class="text-center">
                                                <a href="{!! route('coin.name',$coin->name) !!}">
                                                    <img src="{!! $coin->icon_url !!}" alt="{!! $coin->name !!}" width="20">
                                                    <strong>{!! $coin->name !!}</strong>
                                                </a>
                                            </td>
                                            <td class="font-w600">{!! number_format((float)$usd_24h_vol[$coin->name], 2, '.', ',') !!} <i class="fa fa-usd"></i></td>
                                            <td class="d-none d-sm-table-cell">{!! number_format((float)$market_cap[$coin->name], 2, '.', ',') !!} <i class="fa fa-usd"></i></td>
                                            <td class="d-none d-sm-table-cell">{!! $usd[$coin->name] !!} <i class="fa fa-usd"></i></td>
                                            <td class="d-none d-sm-table-cell"><span style="color: {!! $usd_24h_change[$coin->name] < 0 ? 'red' : 'green' !!}">{!! number_format((float)$usd_24h_change[$coin->name], 2, '.', ',') !!}% <i class="fa fa-angle-double-{!! $usd_24h_change[$coin->name] < 0 ? 'down' : 'up' !!}"></i> </span></td>
                                            <td class="d-none d-sm-table-cell">{!! number_format((float)$roi_percent[$coin->name], 2, '.', ',').' % / '.(int)$roi_days[$coin->name] !!} Days</td>
                                            <td class="d-none d-sm-table-cell">{!! number_format((float)$mn_worth[$coin->name], 3, '.', ',') !!} <i class="fa fa-usd"></i></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="opacity-0">
        <div class="content py-20 font-size-xs clearfix">
            <div class="float-right">
                Developed by <a class="font-w600" href="javascript:void(0)" target="_blank">GG Dev</a>
            </div>
            <div class="float-left">
                <a class="font-w600" href="https://goo.gl/po9Usv" target="_blank">Copyright</a> &copy; <span
                        class="js-year-copy">2019</span> {!! config('app.name') !!}
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
<script src="{!! asset('assets/js/core/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery-scrollLock.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.appear.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.countTo.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/js.cookie.min.js') !!}"></script>
<script src="{!! asset('assets/js/codebase.js') !!}"></script>
<!-- Page JS Plugins -->
<script src="{!! asset('assets/js/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') !!}"></script>
<!-- Page JS Code -->
<script src="{!! asset('assets/js/pages/be_tables_datatables.js') !!}"></script>

<script src="{!! asset('assets/js/plugins/chartjs/Chart.bundle.min.js') !!}"></script>

<!-- Page JS Code -->
<script src="{!! asset('assets/js/pages/be_pages_crypto_dashboard.js') !!}"></script>

</body>
</html>
