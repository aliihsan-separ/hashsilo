@extends('layouts.app')

@section('content')
<div class="col-md-8 offset-2">
    @include('layouts.alerts')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add New Coin</h3>
        </div>
        <div class="block-content">
            <form action="{!! route('coins.store') !!}" method="post">
                @csrf
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="name">Coin Name</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Coin Name" value="{{ \Request::old('name') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="ticker">Coin Ticker</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="ticker" name="ticker" placeholder="Enter Coin's Ticker" value="{{ \Request::old('ticker') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="paid_rewards_mn">Paid Rewards For Masternodes</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="paid_rewards_mn" name="paid_rewards_mn" placeholder="Enter Paid Rewards For Masternodes" value="{{ \Request::old('paid_rewards_mn') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="mn_required_coins">MasterNode Required Coins</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="mn_required_coins" name="mn_required_coins" placeholder="MasterNode Required Coins" value="{{ \Request::old('mn_required_coins') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="icon_url">Icon URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="icon_url" name="icon_url" placeholder="Enter Coin's Icon URL" value="{{ \Request::old('icon_url') }}">
                    </div>
                </div>
                {{-- MN Info--}}
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="masternode_reward_per_block">Masternode Reward Per Block</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="masternode_reward_per_block" name="masternode_reward_per_block" placeholder="Enter Coin's Masternode Reward Per Block" value="{{ \Request::old('masternode_reward_per_block') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="block_time">Block Time (second)</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="block_time" name="block_time" placeholder="Enter Coin's Block Time (second)" value="{{ \Request::old('block_time') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="algorithm">Coin Algorithm</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="block_time" name="algorithm" placeholder="Enter Coin's Algorithm" value="{{ \Request::old('algorithm') }}">
                    </div>
                </div>
                {{-- Deamon Info--}}
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="rpc_host">Deamon Host</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="rpc_host" name="rpc_host" placeholder="Enter Coin's Deamon Host"  value="{{ \Request::old('rpc_host') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="rpc_port">Deamon Port</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="rpc_port" name="rpc_port" placeholder="Enter Coin's Deamon Port" value="{{ \Request::old('rpc_port') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="rpc_user">Deamon User</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="rpc_user" name="rpc_user" placeholder="Enter Coin's Deamon User" value="{{ \Request::old('rpc_user') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="rpc_password">Deamon Password</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="rpc_password" name="rpc_password" placeholder="Enter Coin's Deamon Password" value="{{ \Request::old('rpc_password') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-9 ml-auto">
                        <a href="{!! route('coins.index') !!}" class="btn btn-alt-danger offset-5"> Back</a>
                        <button type="submit" class="btn btn-alt-primary offset-1"> Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection
