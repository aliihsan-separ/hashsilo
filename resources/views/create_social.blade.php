@extends('layouts.app')

@section('content')
<div class="col-md-8 offset-2">
    @include('layouts.alerts')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Social Media</h3>
        </div>

        <div class="block-content">
            <form action="{!! isset($coin->social) ? route('social.update',$coin->social->id) : route('social.store') !!}" method="post">
                @csrf
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="twitter">Twitter URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Enter Twitter URL" value="{!! isset($coin->social->twitter) ? $coin->social->twitter : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="btc_talk">BitcoinTalk URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="btc_talk" name="btc_talk" placeholder="Enter BitcoinTalk URL" value="{!! isset($coin->social->btc_talk) ? $coin->social->btc_talk : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="explorer">Explorer URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="explorer" name="explorer" placeholder="Enter Explorer URL" value="{!! isset($coin->social->explorer) ? $coin->social->explorer : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="github">Github URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="github" name="github" placeholder="Enter Github URL" value="{!! isset($coin->social->github) ? $coin->social->github : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="website">Website URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="website" name="website" placeholder="Enter Website URL" value="{!! isset($coin->social->website) ? $coin->social->website : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange1">Excange URL 1</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange1" name="exchange1" placeholder="Enter Excange URL 1" value="{!! isset($coin->social->exchange1) ? $coin->social->exchange1 : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange1">Excange Name 1</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange_name1" name="exchange_name1" placeholder="Enter Excange Name 1" value="{!! isset($coin->social->exchange_name1) ? $coin->social->exchange_name1 : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange2">Excange URL 2</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange2" name="exchange2" placeholder="Enter Excange URL 2" value="{!! isset($coin->social->exchange2) ? $coin->social->exchange2 : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange_name2">Excange URL 2</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange_name2" name="exchange_name2" placeholder="Enter Excange Name 2" value="{!! isset($coin->social->exchange_name2) ? $coin->social->exchange_name2 : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange3">Excange URL 3</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange3" name="exchange3" placeholder="Enter Excange URL 3" value="{!! isset($coin->social->exchange3) ? $coin->social->exchange3 : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange_name3">Excange Name 3</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange_name3" name="exchange_name3" placeholder="Enter Excange Name 3" value="{!! isset($coin->social->exchange_name3) ? $coin->social->exchange_name3 : null !!}">
                    </div>
                </div>
                <input type="hidden" name="coin_name" value="{!! $coin->name !!}">
                <input type="hidden" name="coin_id" value="{!! $coin->id !!}">
                <div class="form-group row">
                    <div class="col-lg-9 ml-auto">
                        <a href="{!! route('coins.index') !!}" class="btn btn-alt-danger offset-5"> Back</a>
                        <button type="submit" class="btn btn-alt-primary offset-1"> Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection
