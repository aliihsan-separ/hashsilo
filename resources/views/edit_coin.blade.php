@extends('layouts.app')

@section('content')
<div class="col-md-8 offset-2">
    @include('layouts.alerts')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add New Coin</h3>
        </div>
        <div class="block-content">
            <form action="{!! route('coins.update',$coin->id) !!}" method="post">
                @csrf @method('PUT')
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="name">Coin Name</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Coin Name" value="{!! $coin->name !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="ticker">Coin Ticker</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="ticker" name="ticker" placeholder="Enter Coin's Ticker" value="{!! $coin->ticker !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange_api_url">Exchange API URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange_api_url" name="exchange_api_url" placeholder="Enter Exchange API URL" value="{!! $coin->exchange_api_url !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="paid_rewards_mn">Paid Rewards For Masternodes</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="paid_rewards_mn" name="paid_rewards_mn" placeholder="Enter Paid Rewards For Masternodes" value="{!! $coin->paid_rewards_mn !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="mn_required_coins">MasterNode Required Coins</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="mn_required_coins" name="mn_required_coins" placeholder="MasterNode Required Coins" value="{!! $coin->mn_required_coins !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="circulation_supply_api_url">Circulation Supply API URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="circulation_supply_api_url" name="circulation_supply_api_url" placeholder="Enter Circulation Supply API URL" value="{!! $coin->circulation_supply_api_url !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="mn_count_api_url">MasterNode Count API URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="mn_count_api_url" name="mn_count_api_url" placeholder="Enter MasterNode Count API URL" value="{!! $coin->mn_count_api_url !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="icon_url">Icon URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="icon_url" name="icon_url" placeholder="Enter Coin's Icon URL" value="{!! $coin->icon_url !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-9 ml-auto">
                        <a href="{!! route('coins.index') !!}" class="btn btn-alt-danger offset-5"> Back</a>
                        <button type="submit" class="btn btn-alt-primary offset-1"> Edit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection
