@extends('layouts.app')
@php
    if(isset($coin)) {
        $exchange = $exchange_url = json_decode(file_get_contents($coin->exchange_api_url), true)[strtolower($coin->name)];
        $mn_count = json_decode(file_get_contents($coin->mn_count_api_url), true)['result']['active'];
        $daily_reward = $coin->paid_rewards_mn / $mn_count;
        $roi_percent = 365*100/($coin->paid_rewards_mn/ $daily_reward);
        $circulation = json_decode(file_get_contents($coin->circulation_supply_api_url), true);
        $mn_worth = $coin->mn_required_coins * $exchange['usd'];
        $daily_reward_usd = $exchange['usd'] * $daily_reward ;
        $mounthly_reward_show = $daily_reward*30;
        $yearly_reward_show = $daily_reward*360;
    }
@endphp
@section('content')
    <div class="row">
        <div class="col-md-4 float-left">
            <div class="block block-fx-shadow">
                <div class="block-content">
                    <div class="col-12"><h3>{!! $coin->name !!}</h3></div>
                    <hr>
                    <div class="col-12">
                        <img src="{!! $coin->icon_url !!}" alt="{!! $coin->ticker !!}" width="200" style="margin: 10px;">
                    </div>
                </div>
            </div>

            <div class="block block-fx-shadow">
                <div class="block-content">
                    <div class="row">
                        <h5 class="text-center" style="margin-left: 0 !important; padding-left: 20px;">Information</h5>
                    </div>
                    <ul class="list-group" style="margin-bottom:15px !important;">
                        <li class="list-group-item text-left"><i class="fab fa-btc mr-2"></i> BitCoin :  <span class="ml-2">{!! $btc !!} $</span></li>
                        <li class="list-group-item text-left"><i class="fas fa-chart-line mr-2"></i> Volume : <span class="ml-2">{!! number_format((float)$exchange['usd_24h_vol'], 2, '.', ',') !!}$ </span></li>
                        <li class="list-group-item text-left"><i class="fas fa-hand-holding-usd mr-2"></i> Price : <span class="ml-2">{!! $exchange['usd'] !!}$</span></li>
                        <li class="list-group-item text-left"><i class="fas fa-boxes mr-2"></i> Collateral : <span class="ml-2">{!! number_format((float)$coin->mn_required_coins, 2, '.', ',') .' '. $coin->ticker !!}</span></li>
                        <li class="list-group-item text-left"><i class="fas fa-funnel-dollar mr-2"></i> MN Worth : <span class="ml-2">{!! number_format((float)$mn_worth, 2, '.', ',') !!}$  </span></li>
                    </ul>
                </div>
            </div>

            <div class="block block-fx-shadow">
                <div class="block-content">
                    <h5 class="float-left">Social </h5>
                    <table class="table table-vcenter">
                        <tr>
                            <td style=" padding: 15px;" align="left"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABmJLR0QA/wD/AP+gvaeTAAAPDElEQVR4nO2de3gc1XXAf2dmJdvSrmz8wDyMrRcmqfvRBIF5pK6dkIZXTCDBTtIYCPDZYqXifIFCKJDECSEm8DW0xpYsG/BX+PiSCggYCA0tD5uvkEAtHklNeciSDAaDLGyk3ZUsaWdO/9DDM6NZaWdXslNpf39JZ+495849e+/cOTP3DOTIkSNHjhw5cuTIkWOiIUe6AcMx55726XlJs0Jt+xQRKUOZC8xFmY6QD0ztL9qO0iPCJ4q+B/Keok0GRkOSZMN7VdMOHMHTGJY/KwccV6cFk7VziVr2eQjnACeOkup3gKfFMH7X01nw/J5rpWuU9GbNkXfAGjVKZ8XOUpFLgW8DkTG22AU8ieoDzbMiT7FcrDG2NyxHzAHl63SSZSa+ieiNwGePRBsUdqFyd7K7cNORGhWH3QEL6jU/8Un8alFuBo4OrED5GNiE8EOP/FaEyox1GvxsuhGua6iU3sD1s8A4nMaKazou6myL7xTlX0jdUQeAN/wOiFJXQFd5c3XkR95jzdWRHxVoVxnC5hR63+jX7aOY2Sh377fi/1NS03HhyGcyehyWEVCyOT6bpK5HuSRFkR6Ex1X1QcR6XTT0e+AYx/FuRSpbqsL/OqizJqZOBc1VkcFzKa3tuFJVaoF8R5G92KEz1ej9vCArgKWe404eMoS/3xWNtAY5z0wY8xFQvL7ja/TqzhSd341QI2boxOZoZFlLNLJVNLQFd+fbCJc5O38kmqJF94nKdwGnk45VI3lfSzSytbkqcgl2aL7ARqDbR8UyW3nzcIyGMRsBFXWatz8ZW4vItT52LIS6Xsyf74kWfDAgLK6JXS1Q62qgyrVN1eG7vPqHGwGHynRcD3KHR1zZXBXZNPBP+brOOcmQdZPAKsD0lFURuXPux4U3b1sjyWFPOEPGxAGldfun2lbebwS+5HP4dVFjVVN14X87hSWb47Pp1bc5dHMFwtbmaOQiPxvpOKCvXPxJ0AsconZDmO+dXkpq2xeixmbgZB81z5jJnm80rp7R4WcjG0Z9Cjrh7sRxauW94NP53YreOK81fJq38wG0V3+Ms/PhQDJpVGXbnl4xKoFPHaKptu1ZQQHN0amvTDfDpwp6E9DjOfxlK5S/fV5d4ths2+NlVEdA2YbOE2yxt4OWeA7ttQ39xu6ri37vV690Y/uJahs7gbxBoeqq5uqiVCuatEcAQHFtrFKUjQ5Rr2lan22snLbL9zxqO75gqzyM+1qEwq6kmIud02a2jNoIKKuNHW2L9bS380V4VczQGak6H0Bt4wacnQ/vztsX2TJabWuZEb6HvnDEAHm2Zf5DqvK7okUvJi2jAviDUy5QlqfW86M5EkbFAeXrPimylWfx3tEKW43e8FlNlVPeS1W3ZHN8NrDCVU3kJ6N60VsuFiq3OUUKl5evi81KVeX9awo/NJPhJcDjnkMnGpb9u5Pu3TcqIZPsHVCvZtLM+xXwly65sLVgRnh542rxW+YdIqmrgMkOydtNMwp/nXW7PDTPKnwQaHSIplgmq4ar07haugtmhpcx1Akn93RPeZA1mnX/Za2gpC2xVkTOdwn7O3/ncvFezNyoCipXuKqq1I1JgKxvFNS5zAtXoDrsdXDncunxd4IuLT068fNsm5XVRbh4Q/xcEX0qWz3/j1FUzm2uDv9Hpgoy7riy2tjRtvIGnpXCBGSvmeSvGldH9mVSOeMpSPsCahO98wGOtUL6y0wrZzQCStfHz1ZDn8nU6DhEbeXs3dWR54NWDOyAJWs0tHtW/E8In3GIX5/XGj4tyNKxpCZ2K3CLoyGPNVVFLg5QP+0bMZ+6W4HBQJsoP2mqjqxJt35Fnebtt+I7cIQtBN5smhk+OegCIvAUtHtW7HJP51tiGCuDrtt1SKhCvUu9sUPdtlR8Y1YpaaiUXsReCQx2tsJfFO+LXRq0KYEcsKBe8xHjFpdQqW26unBHED0n3bsvIrDQKbPFCjx8M8W27ec8otNn3/lRYRAdzdGpryC4lrUi8sOKOs1LVcePQA7oaot9B7TYIeruNczbg+gAOHhw8qlAaOB/gQ9aoke1BNWTKbuvmdYM7HWI8qcUFp4aVI/Za67F/TyhdH8y8e0gOgI5QBV3dFK4N5PAlIi6fv2qvBhUR9aI26aonBZURePqgj0K97n0GPq9IDrSdkDx+vbTEXH+SnqV5J1BjA0gbj0o8lImerJBVTxO18AOADDVXIsjfK3KKSW17QuHqZIjR44cOXIAadwJL774t3uA411ClR9sf+z8wbcNlix9YqaGjFaHPu0ldMxLj54z+OB78UVP3YDoLzzq92x/9IITJpI9L+msgl4eIhF1vbWseSzC7UzJk54vuOvYJ/nofmUC2nMxogMU9VMy313IWDS0olumyJAG+uke7/a8jOgAEV8vehrI0AbC37j0IJ/xFvDTPd7teRnRAcmktQNH0KmfY7687D+nAixZ9nwY4XM+VT83UObsi38zA3SG57h9sMtomGj2vIzogBcf/1oM+F+v3OrtLgfQZOeZOOI6DsxksudMAFsmD/l1AG++/O/nD3nTbLzb85JmKGLoXGYj/cNU/jplLXQRgKX4XKCGmx/Hu71DpOUAFfG5eA3Mk+o3P/YjiwBEdOgFykfnRLHnJL0R4KNMYH7Fqh15wOkOcSvgjI4uPHNZ/RT1+YUo9pD3QyeMPQdpOWC2Ef8jQsLTwhMjba0VQIFDuk3QbY7/J01OhhcKeOfIg50zj/3TRLXnJC0HPPTQcgvV1zzi+aqe4amyDRFnA7H7Hj2WuopBQ8OmU1PuxRrv9pwEeCAzZJhOBb7ubp+9zbDF82hRL8P94i2Sxh3i+LfXh9/yyhdVXpahkaMzHH9/9MKjF7wFoosv/u37wEAMpNhbSdJYIYx3ewOkPQIMjJGUPg/S96qIsm24gnbSHhp/mWD2DtlNk22PndcCfJyygB66OCm6fRhV+1544sLmiW5vgGDvBQ0T21BTB+dG2yTlKyYaYH4c9/YI6ACxUyrf+8IjS98d+Oe/HvlqE7DbV0eA+XG824OADrCHrhQGjHpfdALwHaaSQsdEtAcBHWDQ/Qpge+W2+g7JbT4yNelO6w5xItjrsxmAbY9d/CnwrlcewhjSGFHDr9GNzz769U9y9nLkyJEjR44AO2T6k1k4b7F7VZLzM3mtvKQmVg8sG/hfke+3VIX/OaCOjHfIAJRuiH9fxbW369+aqyLfCqIDoLSua65ayXdx5B4SNRb65cPwI+1VUHN06iugTqV5Quj69Jt6CFV1begQ9KxM9GSDinre65FAy8dBrOQ/4kr8JC+n2/kQfH9AjVvAVXNqO49PUTy1Hoac7Bd8C44tLqd7fxTpUL6uc46Ca6M5woYgOgI5YEYo8iCIM9A0KQ/rpiA6ACZPPrgDcD6wOK649kBxUD2ZMrfm01LAmXCjpyuRCOwAO2TdDEwa+F9h13QjWJqFQA5oqJRewb7VJVQqSzckAm1uePuqWTEF1ygwMANtlMsGE8Nr6+WPrz8m4Vs4BcXr209XWOmUicpPg2ZdDLxLcm5r5AHc79GYKvbmoJvTBFzxFUUOY7ZCw2VLFL9YT0oq6jRPTGMzjhRnCjv7E4IEIqON2mW1sS/ZyjOZ1h+HqMIXW6oiwz0n8CWjVAW7opHnFH6VSd3xiT6QSedDFrki8k39Hu6tnhOVD/NMrsu0csYOeKeyqI2huTknGrYgl79TWdSWqYKs5/DSmvjtiv7Ao/aJgpmFl6STsKmkNt6I470aQa5rqgqPmH0kkzthnzyijc3R8HxEhv0Rla/TSVYo8Qju9JcosralKhx4Ge4k64xZTTMLbwZ50i3VpZ1tiYcX1Guq1MB99J24KzmfopXUqzeBavbUq6lIpVuoW0bq/AX1mm+FEg97Ox9ha0tr4S0pqqVN9jnjlouVP6nr74A/ug/o0s62+EPFW3Syb72BBgib6MvpP8D8krZg2/3TobgtsUKgzCHqMpOSMi0mQPEWndzZlngY9KueQ68V2F0rWCNDnp4FZVSyJr591ayYIfytwJueQxdKV/ylkvVd81LV3RWNtIpyv1uqP16yRtN+aWxE6tXsT8jq5L7hslzNqe08XroS20CXeg69oyrn76w+Oj4aTRu1vKG7opHWHjG/ouBNhvp5jOQfymo7UsZ7jJB1J+7QRPnuWbErUpUPSnFbfCXubUc9tmX9U6ryJTUdi/JsqwH0dM+hxqRlfLGlOvzRaLVtVFMX74kWfKCmsQh43XPoGFvluZKajpv87pj7M9hucglF7jjh7sRx2bapfF3nHAF3RhdlY3/GFBcVdZpXUhO7BeRZhNnu5vCqqix6/5rCD7Ntk5NRzx29u7Jwr5nsWQx4U5rlg9y234rv8EtmYQg/BdodomkhUzd6ywXFDiU34c5J/alp8TNvueKa9jP2W/FXgVvxvGwLPD3F7lo8mr/8Acbk+wGNq2d0zGsNnycidzD0PuFk1HiptCZWW7ahc3ATc3828xvcRXVpSW0845uc0tqOGxU5z6PzeufcX1rXNbd0Q2yjYLyIN/ksqCJrm2eGLxitOd/LmMdy+j6CIPcCM30O94iyxTaSt7dEj2pBVYpr4894Mq/bKnynJRpxhXlHug8oqYmvAL0f9zk+0xwNfwURnXf3pyWGad4IfBf/L2m0oXplc3XREwFONzCHJZjWn2N0HfDNFEV6RXnSMvQBwXpNNPQS7nh9t4hWNUWLBpMjDeeAkg0dKxFZj88nTESSFQiXKlzA0KkGAIFfG0lWZ5oLNAiHNZpZsqFjKSJ3gt+uwkEOAO/j/yGFe/InHbz27atmxfwcUL7uk6KkOekuEb3Sp+4bwFzgqJSWlbfENK5rurrwqRFPZpQ47OHkijrNO5CMr9S+z1Blkvi1FWEzfZ/BOoRwG8oqIGVG9GHYK8Ktcz8Obx6rT5Wk4ojF8xfUa37nvsS3VPQGgQVHqBmNqrJ+Qn3IzY+ymkSFrfZlCCuA6WNsrgN0q2Dc3xQtfHakWNBY82fhgAHm/FKnhCYlFhuGnqs25yCcRPZtVOAtgadtlaeT3YXbcx/zTJPiuw5MMyeHTrFVKwQptVXnGiLFCtOAKUBRf9EOoEvhAKq7Den/nK1Igxq9DU2V09tTW8mRI0eOHDly5MiRI0eOw8v/AX8QH/Pf50bCAAAAAElFTkSuQmCC" alt="" width="30"> </td>
                            <td style=" padding: 15px;" align="left"> <a href="{!! isset($coin->social->website) ? $coin->social->website : 'javascript:void(0)' !!}" target="_blank"> Web Site</a></td>
                        </tr>
                        <tr>
                            <td style=" padding: 15px;" align="left"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABmJLR0QA/wD/AP+gvaeTAAAIsUlEQVR4nO2dbYwV1RnHf8/s3nuXfYEF1qVAjFAtSwRpUZKaLewuLEqKCKY1polFG2ljqpWmRtt+atImfmih1kI/VFOprUmbqi2FWmzKy8LCbtqElFhEXkQssGJR3hb27u7s7p2nHy7GSuTO3Dvn3Ll3mV/CF+Y5z/Ps+d8zc+a8DcTExMTExMTExMTEXGtI1AnkQrtqG8kMz8WjCaEJaAIagRqQ8aA1WUtJg54H0ginUY6gHEI4jCT2SUvfB9H9FbkpKQF0L9X0J+9AZRGwEJhN+BwV2A90ADtIuFulmYGQPo0RuQCqOOwc04zjrQS+Aoy1HPIiopvw5GXed7fIfWQsx8tJZAJoB1VI6iGEJ4DpEaVxDGUt/e4GWYobRQJFF0D3Uk069QjwODC52PGvwnsoa6l1fynz6C9m4KIKoDtTdyOsA6YVM24e9CD6HWkZeqVYAYsigHZUTUN0PcKyYsQzwGYqnMdk/sAJ24GsC6CdyXtQZwPoeNuxDHMR9BvSOvSSzSDWBNAtpKhJ/QRYbStGkXiOtLva1kPaigC6rW4iCfdVkNtt+I+ALhLucmnmnGnHxgXQPdVTyGT+Btxi2nfEHARnibQOnDTp1KgA2pGciSN/B6436beEOEEFd8p897Aph8YE0O1jppLwulBuMOWzRHmXEfmCtA8eN+HMMeFEt9VNpNLbeg1UPsBUKnWLdjPBhLPQLSDb20nutPbA/fyRT/7/f84ozM4Y2k16aFHY3lH4FlCT+uko6u3kgTRf7maHIpQA2pm8F3g0bBJlzGrdlfxSGAcFC6AdVdNQ+VWY4KMDeV63VxX87Cu8BTi6DhhXcPnRQz2V+vNCCxckgHYm7wHuLjToKGSF7kwVVB9594Ky04apN413Oa/WiwnL0cehdw+MXLDj/yPeIeHOyne6M/8W0J96tKz6+zc9Dbd2QdOzUN9mM9J0RlIP51sorxZweYTzGDAl30C+2GoBV3KhE47/CAatDPW/h+d+WhYyGLRAfi2gOrUKG5VfTOpbYNYrUPtZG94n46QezKdAYAFUcS5PoJc/lfXQ9Dykppr3LTypGvzOErwF7KpqJbrVC+apHAvTfmjer3IjnVULAqcR2LHjrUQjXEYUZEynohaSjVBzCzQsh3E+9VDfAnXz4NJeMzl+iOpKoDOIaaAWoN2MQSXUK3dRyPTBwDE4swkOrYK3vwd4uctMWGI+D+E+7WZMENNgt6CR5J2U41vvmY1wZnNum/oWG5HHMpJaFMQwmADZtZrliZ8AiUl24ioLg5gFfQgHclaSDL6T+3pFNVTU2IjcHsTIVwDtqm0ku0q5PFGfZwCAGJkYvJI52lHX4GfkHzkzPJcSWEVdMFXTcl/3BmCkz0ZkB4Y/F8DIB48mI+lEReOXc19PHyC7hcACjvrWnb8AUsYCTPoqTPQZJT77mr346l93/i9iwgxbPxDjVNRCcjLUzoHr7oW623LbD5+FM3+yl0+AH6+/AMqnjCQTFuOjpQr/+QFk0ob9fgzfugtyC6ozkkpJoXD8KTi31XYg37oL0AKkztpDKgrcU9lf/oVAQzVhMSAAWmsik5LAPZkdHzI9+HZ1fAWw8gZSsqSuh5t/l/1XPTPqbIBAAoiVt5RIqZsHs/4A4wONFoThkp9BgG6oXkLNLEQNRdA1nhXVUHUDjF8Mkx7MTrx8Es4YuOlncPB+6NtvLs+P4yuAfwtQfyclRaYf0gehZz3svwsG3rq6rVMF058CqbCVjQEBhP8aSSUKhk7DkUey4z1Xo3om1LfaysC37oK0gCKtF7HE4HE48+fcNhOX2omt+O6kCSKAse04kXFuW+7rNZa2s4kJAZxRIED/wdzXE4124npiQAAS/6LcX4VHenNfl4SNqB6ZxD4/I18BLh929IaRlKKisj739RHj238BXpfFl876GQV9E94RMploqbs19/VhKwIEqrPRL4CTginfzG3Tf8BGZIMCJNytgM+NtASprIfP/AJqbs5td2GX6ci9JNyOIIaBliZKMwO6S/8I8lC4vGwjkGjITsSPX5idFQty/+/tMp3GS0E3agRfG+o5L+JodALY2j9w8hnzs2KevBjUNPhwdNtgJ3CskHxKlt4u+OBl016P0jq4J6hxYAFE8FDWFpZTCdK3D976FqjpQxNljUjw96b8JmT63Q3AqXxTKil0BE49BwcfsDEh30N68Df5FMhLgMvnIjydV0qlQqYP3v897F8OJ9eCZ+EALGVNvmdHFLZNNZ06gOmTD209ZHvWQ/oNuPiP3MPSYRHeJuPOzmeDXrZYAejO1BcRthRSNm9K9rSUKxCWSYv713yLFTQpL23ua4DPwvtrio2FVD6EWRVR4TwGWN9+XvrIeXC+XWjpggWQ+QMncFhJuQ9Vh0NRb1WYg/xCrQuSBe6rwPowPsoa5RlpG9oYxkX4hVlp97ug3aH9lB+7uc79flgnRna+6FbGkUjtQrCy/7/kUA6QdFtMHORqZGmi3EEvGecuBCNHOZY4PVQ6S02domtsbai0D7yLwxLA+onjEXICh3aTp6obXZwr893DSMXtwL9N+i0R3gRnvixwjb6y2zm8u5sJDCf/AtJsw38E7MZxV8gCzpt2bGV5ujRzDm+oFeTHlPd7ggLraHAX26h8KM4HHFagzq/L8gMOql+XtiHjMzb/j/UNGtIytIkR5lJeY0cbwZltu/Kh2B/x2Z1ahsc6Svfgp2MIqwsdWCuE4n/GqpsxjKQeRnkCsHBmWEH0AGvx3GfzHc8PS3QfcsuewPg1hCdRbowojaMga2gYfEFmMRRFAiVxCIfuTtyG5zwAej/IRMvhehHdTMb5LW2D2/OZQLdBSQjwIdpBFRWpdjzaERYCcwjfUfCA1xE6ULbjuTuKfZvJRUkJcCXaUdeADM1FmAHMBJpQGrO796X+oz3M0gd6AeUSwmngCHAITw6TSewLsko5JiYmJiYmJiYmJiamWPwPcWBw1Y0krKkAAAAASUVORK5CYII=" alt="" width="30"> </td>
                            <td style=" padding: 15px;" align="left"> <a href="{!! isset($coin->social->btc_talk) ? $coin->social->btc_talk : 'javascript:void(0)' !!}" target="_blank"> BitcoinTalk</a></td>
                        </tr>
                        <tr>
                            <td style=" padding: 15px;" align="left"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABmJLR0QA/wD/AP+gvaeTAAAFRElEQVR4nO2cz0scZxzGn+/M7vhrV5MobDRt06zG4kFKl/SXoXjRSyk55lTooWACJUJLQCiUtJfQlkAOQqHQQi/5A4KFSrWHQotQy5L2IphktRZNDVmI0ay77sy8PdQFK67Oj3fmfZd8P0dxZp7hI/vI7jMLMAzDMAzDMAzDMMyzBKkOEJSbc5UBMvElgZpJ4Or469YfqjMFoeEETOaf9ti2eY1A7wMwd3/sCoFbMO2JD8+1PVCZzy8NI+Dr30XrtrNzBYSPAbTX+bUSCJOGY10ff4OexJkvKNoL+FQI49hv1XdB4nMA3R4PWyOiz7qXkt9evEhOlPnCorWAm/PlEQi6QcDLgU5AWHCFmPjoteYpydGkoaWAWsECeEfG+Qg0q2tRayWgTsHKQsui1kKAx4KVhVZFrVRAwIKVhRZFrUxA6IKVheKiNlRcFACeTxtfGCRaVF2/huOKptkl5xNV10+ounCHRbmOTtN9VBK/PiyhT0Bk4ry+K2g9/4+zOLdqD7lANs5r70WZgF2MrlY639lKpdVN++eNCuUApCO+Zml5w53/oWDndhy8FfG1jkS1AAAAQbQ+lzaHM21ifeWJe6ds0xAi+Df00baYu33X7tvaEcOSzx0YLQTUSBqU6T1mZspVUVjZQrHqiFdlnLdkI//93WrqwVNxXsb5ZKKVgBrNScr2H0d2Ywf51U03JQT1BzmPI0Thp2WnuFB0pYiMAi0F1Aha1LoUrBe0FrCLn6LWqmC90AgCABxZ1FoWrBcaRkCNWlFv21T4e9N9vFEW7u171fRDDQvWCw0noEZLQmT7jxPGpiuqo4RC2VsRzH+wAMWwAMWwAMWwAMWwAMWwAMWwAMWwAMWwAMWwAMXEPkupzQ4TBvWcTiPVnAz2YUuNselyqDxdLShcOGsV2y3aUDFfjE1AvV1/U4LmTrdTX9JAoFVEUAFpC+tv9yYWT7YZ/3tbO+75YuQCvO760000f6qNcqbhbxXhV4BlUGn4BWN+oNPMEdX/YCeu+WJkAgLNDgWtd7XRYqYFQyBvqwivAojgnsuYc2+eSvQRed4gRT5fjERA2NkhkSicTBnFE0105IfpXgScPWHkR180U0kjYN9EOF+UKkD2rj9hUP6ooj5MQK1g0xakrCKieM5AioCod/2HFfVBAuoUrLQ8Mos6lIC4d/0HFfVeAR4LVloeGUUdSIDSXf++oh6bLgctWFmEKmrfAnTZ9ROh0J2ix9/c2XFHs8l0kvCSyjxBi9r3WxG67PptV+C7P6vO2CuWYRlC+aNWQZ8z8D1L0W3X32ERdMrj9/iguyDddv265fFMqGGWbrt+3fJ4QcoyTrddv255DkPqNFG3Xb9ueQ4ikm2obrt+3fLsJcpxrm67ft3yAIhhHa3brl+3PLHN03Xb9euSJ/bnA3Tb9avOw6sIxbAAxbAAxbAAxbAAxbAAxbAAxbAAxbAAxbAAxbAAxfgWQAKjBOjwFcALEOKCbnn8HuRbwEhvcvaXM4kcQO8BUPEVwGsALi11pQaXPmif0i2P34ND7Wmm1kRrU8W+QsL/NDHAgxUlAk0aduX6vfHOA6eAuuXxgpRB048roods5xogPI9zfdywC4hbrmlO/HXJ2xhWtzyHIXVRNrNcGYBreJqne7zhWRh0delyKtBrvG55DiKSSd/s/eoICDfEIfvRI254AUJMBHlNbYQ8e4nk39AQxRiq0Bolz14iH7XWK8Z9f3FSCq0R88S2Kt5fjLs3LLXQGjlPbMzcrwzOFKrTZ77anM5Obg1yHoZhGIZhGIZhGIZhGOZZ4V/BsAlfld1s7AAAAABJRU5ErkJggg==" alt="" width="30"> </td>
                            <td style=" padding: 15px;" align="left"> <a href="{!! isset($coin->social->explorer) ? $coin->social->explorer : 'javascript:void(0)' !!}" target="_blank"> Explorer</a></td>
                        </tr>
                        <tr>
                            <td style=" padding: 15px;" align="left"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAPFUlEQVR4nO2de3Qc1X3Hv7+ZlXflpyzLkJAAlgyYUEgocprap5bvrCQ/TuuGlKg8QikOLQ2kND3NoenDadQQAiEnND01eZRSKBgOiZqQNuegWN6dGUsmJiHmkXNMjUnQ+gXlSLIsWbJeO/fXP2ZU0O7sY3bv7kpCn3N85N2593fvznfmzr2/e393gAUWWGCBBRYoDKp0BXIhhIgQ0ToA64joMmb+EIAGIlrCzEsArASwxEs+CmCQiEallCNE9AYRHQHwGjMfjUQiRzo7Oycq9FPyYtYJIoQIEdFHmLmFiFoA/A6AiCLzSQCvEFFMShmrrq7umW0CzQpB2tvbtf379xsAbgHwCQDLylT0WQA/JKLHm5qa7Pb2dlmmcjNSUUGampou1HX9DgA3A7iwknUBcALAE8z8bdu2T1aqEhURpKmpqV7X9b8EcDvUNUeqmALwNIB7Lct6rdyFl1UQT4ivALgegF7OsgsgCeB7zLzLtu1EuQotiyCNjY1Vy5Ytu5OIvgJgaTnKVMgYET0QDofvK0cHoOSCCCFaiOghAJeVuqwS8xoR3WmaplnKQkomiBAipGnaLmb+IgCtVOWUGQbwL3V1dXd3dHRMlqKAkgji9Z6eBrCxFPYrDTO/IKW8vru7u1e1beWCCCG2EdFTcEfQ85nTRHSjaZpdKo0q7ekIIT5FRN/DO66M+Uw1gBsbGhpO9vb2vqzKqDJBotHoXQC+CyCkyuYcQAPw+w0NDaO9vb0/VWFQiSCGYXwZwP2YJa6YMkMAtjQ0NFBvb69drLGiBRFCfJaIvlasnXmAqK+vP5tIJA4WY6QoQQzDuImIHsZ7887wY8uaNWsSiUTilUINFHwivd7UfwOoKtTGPGWSiHYU2vsqSBBvnPESgFWF5H8PMMjM1xTiAws8gm5sbKzyBn0LYmRmJRE93dbWtihoxsCCLF++/H7M0xG4Yj7W399/T9BMgZosz1HYFTSfhwOgWdf1l6WUNcx8OTOvJ6JtcAWutL9rjIjiALoAvCKlPLps2bLhkZGRPwHwzwXaZE3TmuPxuJVvhrxP7Pbt28Pj4+O/ROFe2/+wLOtWvwNCiDUAPktEf47yT1j1AbiPmR+1bfuMz3EyDOMFAI0F2j9SV1f3kXydkXmPqicmJu5GES50z6Xii/fwuzsaje4GcD8z3+CT7Awz92ia9iIzH2XmowD6k8nk0NjY2AgAVFdXLw2FQit0XV8tpbwUwDq4J7IJ6fP0E0T0zXA4/NXOzs7hLFVnAE+icEEuHxgY+Cu4A+ec5HWHeDN9h+H6bwoimUxe0NPT81Y+aaPR6I3M/DCAfgBPEtEPmpqaXi50EYIQIgRgvaZpNzDzTQDGieiTpmn+PJ/8zc3Nm6SU3YWU7XGOiK4wTfNYroR5CWIYxlMAbiyiQlOWZYXhXm15IYSos217IEiefGhsbKxauXLl4lgsNpRvnpaWloscx8l5MnPwhGVZt+RKlFOQaDS6lpmPoDin4YRlWbNtMUPetLa2XpBMJk8VacZh5stt2/5VtkQ5ezbM/Lco3oMb8pqNOQkzq6i7TkR350qUVZCmpqYLAfyRispomvYBBXYqguM471dkamdLS8tF2RJkFcRbxBZ4tOkHM69RYacSEFHWkxiAKinl7dkSZBSkvb1dg7uiUAlzWRBmvlKhrZu9c+tLxgPeWltlyzs1TbtKla0KUOgYxI+Le3p6Nmc6mK3JytlFCwIzb1Jpr1x4nRGldZdSZmx5fAXxvJSfUFkJANcIIebaqkUAWA9guWKbn2xsbPSdR/IVpK+v77egPiRgOBwOK+kglBMimgKgOkxheU1NzXq/A76CEFFUcQXAzF/Yu3fvadV2S41lWYcAfEe1XcdxfM9xpmeIobj8n9m2/Yhim2VD1/W/AzCg0mamiz5NECFEBMAGxYV/A4p9UuUkFosNec5OlWzcvn17OPXLNEG8AMu0hEXw5tDQ0I8U2qsIoVDo23BjRlQRGRsbuzT1S78ma53CQsHM3z906NCUSpuVIBaLHQfwC8VmL0/9wk+QtETFQER5T1/OdojoOcUm0y5+P0GUBtYwczETO7MKKeULKu15j4cZ+AlSr7DM4Qzz1HMSXddVd9vTzrWfIMpGpUT0tipbswEp5TnFJtPOtZ8gytwbzFySsK9KoWma6hF7mjekpHcIgDqFtiqOlHK1YpN5CaLSAViXzfc/1yCi6eWzIwAGAZwC8IbPv/+Fu21HLtIE8ZsrnrHwgYj+EcAZuDvtnJFSntV1fXS6PXUcZ3jRokUO4M49SymXeflWSCm1w4cPz5tQBdM0HwXwaJA8LS0tK4hoycTExJKqqqo6KeW7I63SLlY/Qc4BWDH9QUq527bt/iCVmO/s2LFj8eDgYDgUCi0JhUJpHmxN00YmJyfP2bY94i03GgIAIcQg0YzrczQ1b05BAHzJMIwpuIGc03tTLfX+EoCalPyD3t8zAJiI/ss0zd35/NDZzIYNG6ojkchJALUjIyOoqnKnMxzHSUvrOA6ICIZhAF7rQkTnmHksJWlar81PkBmqeettC4aZf6O9vf1bs2Hro2IIh8NbAdQWkLUGQA2zr291JPWLNEGY+S0iuiRLAf0AEkSUYOYTAPqYuU/TtAEAA8w8quv6IIDk+Pj4Wcdxxue6GABg2/aPNmzYsFjX9QgRRcLhcPXk5GQ4FAqtBLBKSllLRLUA6ojoIm9Rx8UALkDm0MG0pbVpgmiadixl/vv7RLRHStkLIGHbdpqq7xUOHjw4BiC12cmKNyf/QU3TLmLmTwP443cdTlue6tdkpSY6ZZrmj6c/tLS0rGDmi6WUawCsAfA+uNFUq5i5lohWwh3LaHjnWbSSiG43TVP1nELJ8bYcPADgY3CbmCm4sS7DcMUZZOZBIhokokFmfhvAKWZ+U9f1N3VdP9bV1ZUAkDAM49oU83kJ8uuUzx+PRqP107eg4zgZt8xI6UHMgJkfjEajpmmaqfZnO38PVwxg5hjt/we907/73c8JIoKUElJKGIbxFoDXkeK7Yua0c5HWD2bmF1O+amDmawFcjeL2L1kK4Jnm5uY5E5toGMZ1RLRLgan3w41RmbHOTUqZeq59BTmMgO1kvjDzVVLKvUKI1K7yrMMwjN8F8BRKt1XI6Pnnn/9q6pdpgti2nQTwUokqAQCNRPSz5ubmD5ewjGIgIcTnADwDReua/WDmFzs6OtIGMb5+pjJMKl0mpXw+Go3e0dbWNmv2XoxGox8wDOMZIvomSrwhgqZpvufYVxBd158tZWU8qpn5WwMDAy8ZhrG1DOVlZMuWLUui0Wi7F7f48XKUycydft/7to+1tbU/7e/vP43CRqaBYOarAPzEMIxXmfnxcDj8cLkW1BmGsY6Idk5NTd2G8k4VnK6rq3ve70DGfqphGE8CuCnfEpj5Bl3XY1LK6wF8CcB5gavpMkpE+5h5n6ZpsXg8frRAO2m0tbUt6uvr2whgq6ZpW5n5N1XZDgIR7TFN0zcQKqMg3uYyvrdVhkJeJ6Jr4/H4q0KIGiJ6DGpu/7cBHIa7of5hIjrJzANSyoGqqqrT8Xh8xjRxa2vrBVLKOinlaiI6D8DFzHwlEV3BzFdA7ZqzgtA0bUs8Ht/neyxTptWrV++DOwGTF8x8qZRyvxDiEtu2z2zevPkPAPxT8OqmcT6AKIA7iGg33L3aP6Preq2u62luHCIa8uZkPs3Me5j5PgCf8u6GiosB4PimTZvimQ5mnTwSQtxHRH8TsMBfDg8Pr/cWx1E0Gn2cmVVFYv2KiK43TTNtQOWHEOJqIvpPAGsVla+CeyzL+odMB7NOr3rLJ4OuOvzwihUrPuf9n1etWnUbgFhAG378PJlMbsxXDACwbftlZv5tAL4P0AowQUTfzZYgqyCxWOw4ET0ZtFRm/usdO3YsBoCOjo7JSCRyHYC8dk3IwBtTU1Pbenp6+oJmtG27X9O030O6j64SPG6aZtbHQM4FCMlk8gEED1hZffbs2bbpD52dncPj4+OCiJ4OaAcARpj5ugMHDgzmTupPPB4fgNvBKIlLKE8cx3G+nitRzlHysWPH+uvr69cBCBS0SUTnJRKJf5/+fPLkyWRvb+8P1q5d2wXXLV8DYDHci2ISrjt7CMBxAP8D4Flmfqi6uvpPY7FYsdtaIJFI9NXX1+tQH/uSL3v279+fM0Ym381nLtR1/QjcExiEqy3LKnhDSNV477N6FWqXy+bDKBGty9VcAXluGtbd3X2CiHLebj7cVUCekmHb9jgRqeiKB4KI7s9HDCDALm6hUOjrCP5gvEkI8cGAeUrKxMTEY/CW5ZSJo2NjY9/IN3HegnR1dY0y860I9oCvJqJHZtPGM88999xZAD8pU3EOEe305uLzItAyT9u2DzDzgwErtYWIfiyEeF/AfCWDiPaWqagHTdMMtCd8IVfuFwEIuAH1+bKNiF43DOOHcN/rdBTASdu2xwsov2iI6FCGdVLKYOYXAGQckWeimI2Uf4HCPbo5qaqqWtrV1ZW21FIFW7durZ2cnFQa5pzC247jfLS7u/tE0IwFrUzv7u4+oWnaDVAblTqDUokBAHv37i14kJkHU0T0h4WIARSxV248HreYeSfUbztRDkrVXjER/ZlpmgVPgRcVu2Hb9h5m/otibMwzPu+FLBRM0cE0tm0/BHeG8D0NM++yLKvoQaeS6CbLsr5MRHdhbjZfxcIAPm/b9r0qjCkLNzNNczcR3YoSPuhnIVMAbrYsK+jYLCNK4/9M03xC07Qo3Bi7+U4fEW2zLOsplUaVB2TG4/EeIlqP2TNLVwpeJKKPluI1rCWJkDVN81QkEhEAHoC7dH++4BDR1yKRyMZ89nEvhJI5/bw3K39BCPEMET0KxZvaVIBfa5q2Mx6P95SykJLHkNu2/fz4+Pg1cP06JRt9l5ARZt7FzFeWWgygTG/l9NzP97S2tj7iOM69zHwLsl8M5QjDHkD292hJAI8lk8ld+b5mQwVl3WVh3759b5qmuVPTtA8R0b8ByPTC+IwLyRSS6YE8AeBfmXmdZVm3lVMMoMIvhPReA/EZuIGQ0/ur9xHRhlKHvgkhLiGig3hnkfVxuHfEd8otwruZFdtetLe3a7ZtR4lou67rD8VisTfKUa73bpQ7ATy7efNmaz6Eby+wwAILLLDAAgss4Mf/AVUSh1zH79P6AAAAAElFTkSuQmCC" alt="" width="30"> </td>
                            <td style=" padding: 15px;" align="left"> <a href="{!! isset($coin->social->github) ? $coin->social->github : 'javascript:void(0)' !!}" target="_blank"> GitHub</a></td>
                        </tr>
                        <tr>
                            <td style=" padding: 15px;" align="left"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABmJLR0QA/wD/AP+gvaeTAAAIB0lEQVR4nO3cf4wcZR3H8fd3ZvdKb/ZarFKLaClIVAiEElJIi6AFwo/SVsPB3pUQCIjWAqaJGiAqphoImGAqNZa0MZAqeLdLqpHAtSnBAoEoSoo0hJYq2PDLekBtb3e2P3af+frHNdJee+3dPM/sXpvn9cf9Mdn5fL83z87OzLMzC57neZ7neZ7neZ7neZ7neZ7neZ53LJNWN5C5Ph0XDtQuRpKvQXAmmpyIyIkM/u//RXhDlL+Crm1sKjzHEkma2d6xOwCP7fhELpe7S2ER0DHCtbai/NRsjlaNeCDKA18MNLw26YruSdNmkGalAxvQNh79aIJ1jkNBubIozOXeVLiDkW98gGkIDwdnxC/nSgMXHO6F+d54Rliq/jbU4DVRtqXt1XoPCHrjhSJaNB3RHObIHts8K306LqzEy4GbHaQ1FP1e0tWx7P9LyrumBmo6BbqB8/Ytfd9IdBpF2ZWmiN0AqEpYjt8CpoGsNtLeRVGMVWZaZW0LiftQLnGaq/IQkoggFymcztBtpjLPdEdPpo3P2fSWe7xygRJM29dJZ6jVXxnVRYioTW4aYRIvRxxvfADRRSAM8w89YrPxwfYYoMHcAxfIwrAcP8IKzVvljlJYjr+F8I1m1gT6jETfPmBJiv/bagAUZhxi8Y3h8fETlPsLNtkjVt4+EdV7m1LrY0+ZWtRJUfYC5Hvic3Kl6gPB8fHtow2y+ggCThtm+RUh7evN6ng+ndG/LWscVk7b7lT4VJY19lNX4cdJYFaH4+NOSpUvg1yRoNNAXkwmt391tIFWB+GwVB3g8Kd5/SA3ma6oz6bOsNZrLuyPtwGfzCT/YBUgDxw3ZPlWk5NZad5sttcBR1p/MuiTud7qL+jTcZa1DpLrr15I8zY+DL7Zhm787UZ1Tto93XYAdozgNaLC4qAS/y1XrnzFst7Q6Plu80btgyCRS+nu2JQ2wHYA/jnSFwqcpSrPhqVqidW7TrasC4AK013kpPS2CZOL6guiV2xCrAZAYEOK1Yphw2zKlar30xN/xqY+Kp+zWj+99SavM7hmwmbbINs9IO3BdbzCnWGg/wpL1VX53urZ6WI0Slnfxp/M5Ogyru7odxFmNQCNydGzwPsWEW3ADYnw97BcfT7orXyHcu2kUay/26J2Wv3MloarMLs9YLY0FJY66US5UESWhZq8HZYrLwal6vdzPQOzKOv4w6z1gZPao2M/g7wf2wsxkvZoeViLbwVOcdAPQIDKLIFZGgSEGtcpVTeCvKSiWwR9T4y+3wjz76JmC4e+Gs9S7DLMegCYJzUpVxeqshbH74598sC5oOcOTvEJGgihtmbSFaTqMs3JBmsUC0+r8EMXWWOdkFRc5qUfgEc/mhCW4/moCkBSLNwvcJ+zzsaoxOLbr0NJ/xFUn7SXfPzHXDneQqnSI0m4pi7jlwRU3xGVpYDzqYexQAhGfPE5sjwLYam6E9j/++C9qL6LSAdwglVnY5QJzRe4ZuI/XOXZHQOELUOWtCFyKsfoxgfqfDRhq8tAu6kI1XWuGjkqCBtYKHWXkZZ7gK5x1MdRQRJedJ1pNxVRnPACwsuumhnrNAhecJ3p4DpAf2afcVQwBjP2BsBcW1gNPOWgl7HuOYodzuee7PcAETXCLbRmYqxpFP19Frlu5m6KhW1BIpcD253kjT1JkgR/yCLY2eRZfUH0SpAEc4APXWWOIWtZENl87zEsp7OX9QXtLxnDmQjH1OmpwLIjvyod99PH1xX+Y16P5qJcL/C68/wmE2VzoxhldsGZxfw9LJHEdBcea2yKzhLhSpDlwDuZ1MpYIvJgljcb238hM0TYG/8S9ASBbUot1IRJGujpokxxXSt78mYi7Q9nWcH5ACBsBFYOvmUG/zb/ZnVHNPkRXYM34GbF+UeQ2dP+O+A917nNprDBdBVKWddxfwy4QWLQ7zrPba4kkGRxMx40yeQgbLo6ykBPFtnNILC0UZzgfN7nULI5CwJMLboZ9M9Z5WdFYEtDorubVS+zAeAm2W2kfiXCM5nVcG+PJMENaZ94TCO7AQAoTtppiOaI8iDQ1CfQU1G5vb6g/aVmlmzak/L5ntr5SaA/Bz3sA9Ato/KQ6Y5ubXbZpv9UQb5cOy9RcwvIpbi7ndHW02ZHdJXr73tHorW/FbG69tlco3GyIne08GmX9aY9mss8qbWieLbHgCORxi6VcHELN/7zZk80r1UbH1q1ByzRIPhS7Zsiei/NfcjuY8IaQ61IcbLTm21H30YzLdEgPL12lYr+RJRzmlp7f6orzacLt7l80CKt5gxAuTolUG4S1Vv23TnXKntRuc10R79uYQ8HyGYAVmg+P7E23YjOFmQe6EwgzKTWCCm8Gio31rsLr7ayj6FGPQC5nuolSSDnI8mAJMFuAgJFJwYJkxROQTgVOIuDH2hulbqo3tfYWbinFaeZR5JqDwhL8deBB0A/77gft4Q1YYO79l5X2NjqVoaT/iNohebDifECRO4GHe5HO1pE/yLwg0ZXx/pWd3Ik9seAwYEoInobyEwHPaW1F+QJEV3ZKBaebmEfo+L0IJwvV6ercr3CtcBUl9nDUXgNWJXk9TeuHp5upmzOglQl//iuGUb1MkEvBmbi7qD8IcoziKwzOVlHZ/u7jnJboknXAdqWJz4jUc4WOFNhKshJwBTQSQzeHNAx+AioVkSpqjAAbAd9S1U2B/B6Ix++wdXHvd2K36TzPM/zPM/zPM/zPM/zPM/zPM/zPM/zvNH6H/aDn6CDopMQAAAAAElFTkSuQmCC" alt="" width="30"> </td>
                            <td style=" padding: 15px;" align="left"> <a href="{!! isset($coin->social->twitter) ? $coin->social->twitter : 'javascript:void(0)' !!}" target="_blank"> Twitter</a></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="block block-fx-shadow">
                <div class="block-content">
                    <h5 class="float-left">Top Exhange</h5>
                    <table class="table table-vcenter">
                    @if(isset($coin->social->exchange1))
                            <tr>
                                <td style=" padding: 15px;" align="left">
                                    <a href="{!! $coin->social->exchange1 !!}" target="_blank"> {!! $coin->social->exchange_name1 !!}</a>
                                </td>
                            </tr>
                    @endif
                    @if(isset($coin->social->exchange2))
                            <tr>
                                <td style=" padding: 15px;" align="left">
                                    <a href="{!! $coin->social->exchange2 !!}" target="_blank"> {!! $coin->social->exchange_name2 !!}</a>
                                </td>
                            </tr>
                    @endif
                    @if(isset($coin->social->exchange3))
                            <tr>
                                <td style=" padding: 15px;" align="left">
                                    <a href="{!! $coin->social->exchange3 !!}" target="_blank"> {!! $coin->social->exchange_name3 !!}</a>
                                </td>
                            </tr>
                    @endif
                    </table>
                </div>
            </div>
        </div>

        <div class="col-8">
            <div class="content" style="padding: 0 !important;">
                <div class="text-center">

                    <div class="row js-appear-enabled animated fadeIn" data-toggle="appear">

                        <div class="col-4">
                            <a class="block block-link-shadow text-left" href="javascript:void(0)" style="border-top: 3px solid green;">
                                <div class="block-content block-content-full clearfix">
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="font-size-h3 font-w600  js-count-to-enabled">$ {!! number_format((float)$daily_reward_usd, 2, '.', ',') !!}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-muted"><i class="fab fa-btc"></i> {!! number_format((float)$daily_reward_usd/$btc, 8, '.', ',') !!}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">{!! number_format((float)$daily_reward,8, '.', ',') . ' '. $coin->ticker !!}</div>
                                        </div>
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Daily Reward</div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-4">
                            <a class="block block-link-shadow text-left" href="javascript:void(0)" style="border-top: 3px solid red;">
                                <div class="block-content block-content-full clearfix">
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="font-size-h3 font-w600 js-count-to-enabled">$ {!! number_format((float)$daily_reward_usd*30, 2, '.', ',') !!}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-muted"><i class="fab fa-btc"></i> {!! number_format((float)$daily_reward_usd*30/$btc, 8, '.', ',') !!}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">{!! number_format((float)$daily_reward*30,8, '.', ',') . ' '. $coin->ticker !!}</div>
                                        </div>
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Monthly Reward</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-4">
                            <a class="block block-link-shadow text-left" href="javascript:void(0)" style="border-top: 3px solid blue;">
                                <div class="block-content block-content-full clearfix">
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="font-size-h3 font-w600 js-count-to-enabled">{!! number_format((float)$daily_reward_usd*360, 2, '.', ',') !!}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-muted"><i class="fab fa-btc"></i> {!! number_format((float)$daily_reward_usd*360/$btc, 8, '.', ',') !!}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">{!! number_format((float)$daily_reward*360,8, '.', ',') . ' '. $coin->ticker !!}</div>
                                        </div>
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Yearly Reward</div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>

            <div class="block block-fx-shadow">
                <div class="block-content">
                    <div class="12">
                        <div class="block-content block-content-full clearfix">
                            <!-- TradingView Widget BEGIN -->
                            <div class="tradingview-widget-container">
                                <div id="tradingview_f2eea"></div>
                                <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/USD-{!! strtoupper($coin->ticker) !!}/" rel="noopener" target="_blank"><span class="blue-text">{!! strtoupper($coin->ticker) !!} chart</span></a> by TradingView</div>
                                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                <script type="text/javascript">
                                    new TradingView.widget(
                                        {
                                            "width": "100%",
                                            "height": 450,
                                            "symbol": "{!! strtoupper($coin->ticker) !!}USD",
                                            "interval": "D",
                                            "timezone": "Etc/UTC",
                                            "theme": "Light",
                                            "style": "3",
                                            "locale": "en",
                                            "toolbar_bg": "#f1f3f6",
                                            "enable_publishing": false,
                                            "hide_top_toolbar": true,
                                            "allow_symbol_change": true,
                                            "save_image": false,
                                            "container_id": "tradingview_f2eea"
                                        }
                                    );
                                </script>
                            </div>
                            <!-- TradingView Widget END -->
                    </div>
                </div>
            </div>
            </div>
            @if(isset($coin))
            <div class="block block-fx-shadow" style="margin:0 !important;padding: 0; !important;">
                <div class="block-content" style="margin:0 !important;padding: 0; !important;">
                    <div class="col-12" style="margin:0 !important;padding: 0; !important;">
                        <table class="table table-striped table-vcenter">
                            <tr>
                                <td style=" padding: 15px;" align="left">ROI : </td>
                                <td style=" padding: 15px;">{!!  number_format((float)$roi_percent, 2, '.', ',') .'% / '.($coin->paid_rewards_mn / $daily_reward) !!} Days</td>
                            </tr>
                            <tr style="padding: 0 20px;">
                                <td style=" padding: 15px;" align="left">Active Masternode : </td>
                                <td style=" padding: 15px;">{!!  number_format((float)$mn_count, 2, '.', ',') !!}</td>
                            </tr>
                            <tr style="padding: 0 20px;">
                                <td style=" padding: 15px;" align="left">Current supply : </td>
                                <td style=" padding: 15px;">{!!  number_format((float)$circulation, 0, '.', ',') .' '. strtoupper($coin->ticker) !!} </td>
                            </tr>
                            <tr style="padding: 0 20px;">
                                <td style=" padding: 15px;" align="left">Required coins for masternode : </td>
                                <td style=" padding: 15px;">{!!  number_format((float)$coin->mn_required_coins, 2, '.', ',') !!} </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            @if(isset($coin->social))
            <div class="block block-fx-shadow" style="margin:0 !important;padding: 0; !important; height: 650px!important; overflow-y: scroll;">
                <div class="block-content" style="margin:0 !important;padding: 0; !important;">
                    <div class="col-12" style="margin:0 !important;padding: 0; !important;">
                        <a class="twitter-timeline" href="{!! $coin->social->twitter !!}">Tweetas by {!! ucfirst($coin->name) !!}</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                </div>
            </div>
            @endif
            @endif
        </div>
    </div>
@endsection
